| #   | Stage              | Start date | End date   | Comment                                                                                |
|-----|--------------------|------------|------------|----------------------------------------------------------------------------------------|
| 1   | Initialize project | 2023-06-02 | 2023-06-03 | Create repository, create project, first user interface, realize Class Factory pattern |
| 2   | Create entities    | 2023-06-03 | 2023-06-04 | Create entities for clothes and footwears with one globsl abstarct class               |
| 3   | Data Access Layer  | 2023-06-04 | 2023-06-08 | Implementing Data Access Layer via Abstract Generic DAO.                               |
