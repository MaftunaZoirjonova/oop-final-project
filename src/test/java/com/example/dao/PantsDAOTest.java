package com.example.dao;

import org.example.dal.dao.impl.PantsDAO;
import org.example.dal.dao.impl.ShirtDAO;
import org.example.dal.search.Parameter;
import org.example.dal.search.criterias.PantsSearchCriteria;
import org.example.dal.search.criterias.ShirtSearchCriteria;
import org.example.entities.Product;
import org.example.entities.clothing.Clothing;
import org.example.entities.clothing.Pants;
import org.example.entities.clothing.Shirt;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.function.IntFunction;

import static org.junit.jupiter.api.Assertions.*;

public class PantsDAOTest {
    @Test
    void shouldFindAll() {
        PantsDAO dao = new PantsDAO("pants.test.csv");
        Pants[] expected = createExpectedAllShirts();

        Collection<Pants> collection = dao.find(new PantsSearchCriteria().add(Parameter.any()));
        assertNotNull(collection);
        Pants[] pants = collection.toArray(Pants[]::new);
        assertArrayEquals(expected, pants);
    }

    @Test
    void shouldFindNone() {
        PantsDAO dao = new PantsDAO("pants.test.csv");

        Collection<Pants> collection = dao.find(new PantsSearchCriteria().add(Parameter.none()));

        assertNotNull(collection);
        Pants[] pants = collection.toArray(Pants[]::new);
        assertArrayEquals(new Shirt[0], pants);
    }

    private Pants[] createExpectedAllShirts() {
        Pants skinnyJeans = new Pants();
        skinnyJeans.setId(1);
        skinnyJeans.setName("Skinny Jeans");
        skinnyJeans.setPrice(5999L);
        skinnyJeans.setCategory("Women's Clothing");
        skinnyJeans.setCount(10);
        skinnyJeans.setColor("Blue");
        skinnyJeans.setSize(Clothing.Size.M);
        skinnyJeans.setLength(32);

        Pants chinoPants = new Pants();
        chinoPants.setId(2);
        chinoPants.setName("Chino Pants");
        chinoPants.setPrice(3999L);
        chinoPants.setCategory("Men's Clothing");
        chinoPants.setCount(15);
        chinoPants.setColor("Khaki");
        chinoPants.setSize(Clothing.Size.S);
        chinoPants.setLength(34);

        return new Pants[] {
                skinnyJeans,
                chinoPants
        };
    }

    @Test
    void shouldCreateNewPants() {
        PantsDAO dao = new PantsDAO("pants.test.csv");
        Pants skinnyJeans = new Pants();
        skinnyJeans.setId(3);
        skinnyJeans.setName("Skinny Jeans");
        skinnyJeans.setPrice(5999L);
        skinnyJeans.setCategory("Women's Clothing");
        skinnyJeans.setCount(10);
        skinnyJeans.setColor("Blue");
        skinnyJeans.setSize(Clothing.Size.M);
        skinnyJeans.setLength(32);

        Pants newPants = dao.create(skinnyJeans);
        assertNotNull(newPants);
        Collection<Pants> collection = dao.find(new PantsSearchCriteria().add(Parameter.any()));
        assertEquals(collection.size(), 3);
    }

    @Test
    void shouldDeletePants() {
        PantsDAO dao = new PantsDAO("pants.test.csv");
        assertDoesNotThrow(() -> dao.delete(3));
        Collection<Pants> collection = dao.find(new PantsSearchCriteria().add(Parameter.any()));
        assertEquals(collection.size(), 2);
    }
}
