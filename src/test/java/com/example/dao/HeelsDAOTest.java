package com.example.dao;

import org.example.dal.dao.impl.DressDAO;
import org.example.dal.dao.impl.HeelsDAO;
import org.example.dal.dao.impl.ShirtDAO;
import org.example.dal.search.Parameter;
import org.example.dal.search.criterias.DressSearchCriteria;
import org.example.dal.search.criterias.HeelsSearchCriteria;
import org.example.dal.search.criterias.ShirtSearchCriteria;
import org.example.entities.Product;
import org.example.entities.clothing.Clothing;
import org.example.entities.clothing.Dress;
import org.example.entities.clothing.Shirt;
import org.example.entities.footwear.Heels;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.function.IntFunction;

import static org.junit.jupiter.api.Assertions.*;

public class HeelsDAOTest {
    @Test
    void shouldFindAll() {
        HeelsDAO dao = new HeelsDAO("heels.test.csv");
        Heels[] expected = createExpectedAllHeels();

        Collection<Heels> collection = dao.find(new HeelsSearchCriteria().add(Parameter.any()));
        assertNotNull(collection);
        Heels[] heels = collection.toArray(Heels[]::new);
        assertArrayEquals(expected, heels);
    }

    @Test
    void shouldFindNone() {
        HeelsDAO dao = new HeelsDAO("heels.test.csv");

        Collection<Heels> collection = dao.find(new HeelsSearchCriteria().add(Parameter.none()));

        assertNotNull(collection);
        Heels[] heels = collection.toArray(Heels[]::new);
        assertArrayEquals(new Shirt[0], heels);
    }

    private Heels[] createExpectedAllHeels() {
        Heels expectedHeels_1 = new Heels();
        expectedHeels_1.setId(1);
        expectedHeels_1.setName("Stiletto Pumps");
        expectedHeels_1.setPrice(9999L);
        expectedHeels_1.setCategory("Women's Shoes");
        expectedHeels_1.setCount(10);
        expectedHeels_1.setBrand("Jimmy Choo");
        expectedHeels_1.setSize(7);
        expectedHeels_1.setHeelHeight(4);

        Heels expectedHeels_2 = new Heels();
        expectedHeels_2.setId(2);
        expectedHeels_2.setName("Block Heel Sandals");
        expectedHeels_2.setPrice(7999L);
        expectedHeels_2.setCategory("Women's Shoes");
        expectedHeels_2.setCount(15);
        expectedHeels_2.setBrand("Steve Madden");
        expectedHeels_2.setSize(8);
        expectedHeels_2.setHeelHeight(3);

        return new Heels[] {
                expectedHeels_1,
                expectedHeels_2
        };
    }

    @Test
    void shouldCreateNewHeels() {
        HeelsDAO dao = new HeelsDAO("heels.test.csv");
        Heels heels = new Heels();
        heels.setId(3);
        heels.setName("Stiletto Pumps");
        heels.setPrice(9999L);
        heels.setCategory("Women's Shoes");
        heels.setCount(10);
        heels.setBrand("Jimmy Choo");
        heels.setSize(7);
        heels.setHeelHeight(4);
        Heels newHeels = dao.create(heels);
        assertNotNull(newHeels);
        Collection<Heels> collection = dao.find(new HeelsSearchCriteria().add(Parameter.any()));
        assertEquals(collection.size(), 3);
        dao.delete(3);
    }

    @Test
    void shouldDeleteDress() {
        HeelsDAO dao = new HeelsDAO("heels.test.csv");
        assertDoesNotThrow(() -> dao.delete(1));
        Collection<Heels> collection = dao.find(new HeelsSearchCriteria().add(Parameter.any()));
        assertEquals(1, collection.size());
    }
}
