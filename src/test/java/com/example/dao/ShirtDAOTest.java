package com.example.dao;

import org.example.dal.dao.impl.PantsDAO;
import org.example.dal.dao.impl.ShirtDAO;
import org.example.dal.search.Parameter;
import org.example.dal.search.criterias.PantsSearchCriteria;
import org.example.dal.search.criterias.ShirtSearchCriteria;
import org.example.entities.Product;
import org.example.entities.clothing.Clothing;
import org.example.entities.clothing.Pants;
import org.example.entities.clothing.Shirt;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.function.IntFunction;

import static org.junit.jupiter.api.Assertions.*;

public class ShirtDAOTest {
    @Test
    void shouldFindAll() {
        ShirtDAO dao = new ShirtDAO("shirts.test.csv");
        Shirt[] expected = createExpectedAllShirts();

        Iterable<Shirt> iterable = dao.find(new ShirtSearchCriteria().add(Parameter.any()));
        assertNotNull(iterable);
        Shirt[] shirts = toArray(iterable, Shirt[]::new);
        assertArrayEquals(expected, shirts);
    }

    @Test
    void shouldFindNone() {
        ShirtDAO dao = new ShirtDAO("shirts.csv");

        Iterable<Shirt> iterable = dao.find(new ShirtSearchCriteria().add(Parameter.none()));

        assertNotNull(iterable);
        Shirt[] shirts = toArray(iterable, Shirt[]::new);
        assertArrayEquals(new Shirt[0], shirts);
    }

    private Shirt[] createExpectedAllShirts() {
        Shirt expectedShirt_1 = new Shirt();
        expectedShirt_1.setId(1);
        expectedShirt_1.setName("Usual White Shirt");
        expectedShirt_1.setPrice(57L);
        expectedShirt_1.setCategory("Man Shirts");
        expectedShirt_1.setCount(5);
        expectedShirt_1.setColor("white");
        expectedShirt_1.setSize(Clothing.Size.M);
        expectedShirt_1.setMaterial("cotton");

        Shirt expectedShirt_2 = new Shirt();
        expectedShirt_2.setId(2);
        expectedShirt_2.setName("Unusual Black Shirt");
        expectedShirt_2.setPrice(60L);
        expectedShirt_2.setCategory("Man Shirts");
        expectedShirt_2.setCount(4);
        expectedShirt_2.setColor("black");
        expectedShirt_2.setSize(Clothing.Size.L);
        expectedShirt_2.setMaterial("silk");

        return new Shirt[] {
                expectedShirt_1,
                expectedShirt_2
        };
    }

    @Test
    void shouldCreateNewPants() {
        ShirtDAO dao = new ShirtDAO("shirts.test.csv");
        Shirt shirt = new Shirt();
        shirt.setId(3);
        shirt.setName("Usual White Shirt");
        shirt.setPrice(57L);
        shirt.setCategory("Man Shirts");
        shirt.setCount(5);
        shirt.setColor("white");
        shirt.setSize(Clothing.Size.M);
        shirt.setMaterial("cotton");

        Shirt newShirt = dao.create(shirt);
        assertNotNull(newShirt);
        Collection<Shirt> collection = dao.find(new ShirtSearchCriteria().add(Parameter.any()));
        assertEquals(collection.size(), 3);
    }

    @Test
    void shouldDeletePants() {
        ShirtDAO dao = new ShirtDAO("shirts.test.csv");
        assertDoesNotThrow(() -> dao.delete(3));
        Collection<Shirt> collection = dao.find(new ShirtSearchCriteria().add(Parameter.any()));
        assertEquals(collection.size(), 2);
    }

    private <A extends Product> A[] toArray(Iterable<A> iterable, IntFunction<A[]> arrayGen) {
        ArrayList<A> list = new ArrayList<>();
        iterable.forEach(list::add);
        A[] arr = list.toArray(arrayGen);
        Arrays.sort(arr, Comparator.comparingLong(Product::getPrice));
        return arr;
    }
}
