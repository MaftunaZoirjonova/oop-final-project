package com.example.dao;

import org.example.dal.dao.impl.DressDAO;
import org.example.dal.search.Parameter;
import org.example.dal.search.criterias.DressSearchCriteria;
import org.example.dal.search.parametrs.DressStyleParameter;
import org.example.entities.Product;
import org.example.entities.clothing.Clothing;
import org.example.entities.clothing.Dress;
import org.example.entities.clothing.Shirt;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.function.IntFunction;

import static org.junit.jupiter.api.Assertions.*;

public class DressDAOTest {
    @Test
    void shouldFindAll() {
        DressDAO dao = new DressDAO("dresses.test.csv");
        Dress[] expected = createExpectedAllDress();

        Collection<Dress> collection = dao.find(new DressSearchCriteria().add(Parameter.any()));
        assertNotNull(collection);
        Dress[] shirts = collection.toArray(Dress[]::new);
        assertArrayEquals(expected, shirts);
    }

    @Test
    void shouldFindBohemianStyle() {
        DressDAO dao = new DressDAO("dresses.test.csv");
        Dress expectedShirt_1 = new Dress();
        expectedShirt_1.setId(1);
        expectedShirt_1.setName("Floral Maxi Dress");
        expectedShirt_1.setPrice(8999L);
        expectedShirt_1.setCategory("Women's Clothing");
        expectedShirt_1.setCount(10);
        expectedShirt_1.setColor("Pink");
        expectedShirt_1.setSize(Clothing.Size.M);
        expectedShirt_1.setStyle("Bohemian");
        Dress[] expected = new Dress[] {expectedShirt_1};
        Iterable<Dress> iterable = dao.find(new DressSearchCriteria().add(new DressStyleParameter("Bohemian")));
        assertNotNull(iterable);
        Dress[] shirts = toArray(iterable, Dress[]::new);
        assertArrayEquals(expected, shirts);
    }

    @Test
    void shouldFindNone() {
        DressDAO dao = new DressDAO("dresses.test.csv");

        Iterable<Dress> iterable = dao.find(new DressSearchCriteria().add(Parameter.none()));

        assertNotNull(iterable);
        Dress[] dresses = toArray(iterable, Dress[]::new);
        assertArrayEquals(new Shirt[0], dresses);
    }

    private Dress[] createExpectedAllDress() {
        Dress expectedShirt_1 = new Dress();
        expectedShirt_1.setId(1);
        expectedShirt_1.setName("Floral Maxi Dress");
        expectedShirt_1.setPrice(8999L);
        expectedShirt_1.setCategory("Women's Clothing");
        expectedShirt_1.setCount(10);
        expectedShirt_1.setColor("Pink");
        expectedShirt_1.setSize(Clothing.Size.M);
        expectedShirt_1.setStyle("Bohemian");

        Dress expectedShirt_2 = new Dress();
        expectedShirt_2.setId(2);
        expectedShirt_2.setName("Little Black Dress");
        expectedShirt_2.setPrice(5999L);
        expectedShirt_2.setCategory("Women's Clothing");
        expectedShirt_2.setCount(15);
        expectedShirt_2.setColor("Black");
        expectedShirt_2.setSize(Clothing.Size.S);
        expectedShirt_2.setStyle("Classic");

        return new Dress[] {
                expectedShirt_1,
                expectedShirt_2
        };
    }

    @Test
    void shouldCreateNewDress() {
        DressDAO dao = new DressDAO("dresses.test.csv");
        Dress dress = new Dress();
        dress.setId(6);
        dress.setName("Floral Maxi Dress");
        dress.setPrice(8999L);
        dress.setCategory("Women's Clothing");
        dress.setCount(10);
        dress.setColor("Pink");
        dress.setSize(Clothing.Size.M);
        dress.setStyle("Bohemian");
        Dress newDress = dao.create(dress);
        assertNotNull(newDress);
        Collection<Dress> collection = dao.find(new DressSearchCriteria().add(Parameter.any()));
        assertEquals(collection.size(), 3);
        dao.delete(6);
    }

    @Test
    void shouldDeleteDress() {
        DressDAO dao = new DressDAO("dresses.test.csv");
        assertDoesNotThrow(() -> dao.delete(1));
        Collection<Dress> collection = dao.find(new DressSearchCriteria().add(Parameter.any()));
        assertEquals(1, collection.size());
    }

    private <A extends Product> A[] toArray(Iterable<A> iterable, IntFunction<A[]> arrayGen) {
        ArrayList<A> list = new ArrayList<>();
        iterable.forEach(list::add);
        A[] arr = list.toArray(arrayGen);
        Arrays.sort(arr, Comparator.comparingLong(Product::getPrice));
        return arr;
    }
}
