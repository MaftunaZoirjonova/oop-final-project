package com.example.dao;

import org.example.dal.dao.UsersDAO;
import org.example.dal.dao.impl.UsersDAOImpl;
import org.example.dal.search.criterias.UsersSearchCriteria;
import org.example.dal.search.parametrs.UsernameParameter;
import org.example.entities.users.AdminRole;
import org.example.entities.users.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UsersDAOTest {
    @Test
    void shouldFindUser() {
        UsersDAO<User> usersDAO = new UsersDAOImpl("users.csv");
        User foundUserAdmin = usersDAO.findOne(new UsersSearchCriteria().add(new UsernameParameter("admin")));
        Assertions.assertNotNull(foundUserAdmin);
        Assertions.assertTrue(foundUserAdmin.hasRole(new AdminRole()));
    }
}
