package com.example.dao;

import org.example.dal.dao.ProductDAO;
import org.example.dal.dao.impl.*;
import org.example.entities.Product;
import org.example.entities.clothing.Dress;
import org.example.entities.clothing.Pants;
import org.example.entities.clothing.Shirt;
import org.example.entities.footwear.Heels;
import org.example.entities.footwear.Shoes;
import org.example.entities.footwear.Sneakers;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DAOFactoryTest {
    @Test
    void shouldReturnNullWhenUnknownClassIsPassed() {
        class DummyProduct extends Product {
            @Override
            public Class getApplianceType() {
                return DummyProduct.class;
            }
        }
        ProductDAO<DummyProduct> dao = DAOFactoryImpl.INSTANCE.getProductDAO(DummyProduct.class);
        assertNull(dao);
    }

    @Test
    void shouldReturnShirtDao() {
        ProductDAO<Shirt> dao = DAOFactoryImpl.INSTANCE.getProductDAO(Shirt.class);
        assertNotNull(dao);
        assertTrue(dao instanceof ShirtDAO);
    }

    @Test
    void shouldReturnDressDao() {
        ProductDAO<Dress> dao = DAOFactoryImpl.INSTANCE.getProductDAO(Dress.class);
        assertNotNull(dao);
        assertTrue(dao instanceof DressDAO);
    }

    @Test
    void shouldReturnPantsDao() {
        ProductDAO<Pants> dao = DAOFactoryImpl.INSTANCE.getProductDAO(Pants.class);
        assertNotNull(dao);
        assertTrue(dao instanceof PantsDAO);
    }

    @Test
    void shouldReturnShoesDao() {
        ProductDAO<Shoes> dao = DAOFactoryImpl.INSTANCE.getProductDAO(Shoes.class);
        assertNotNull(dao);
        assertTrue(dao instanceof ShoesDAO);
    }

    @Test
    void shouldReturnSneakersDao() {
        ProductDAO<Sneakers> dao = DAOFactoryImpl.INSTANCE.getProductDAO(Sneakers.class);
        assertNotNull(dao);
        assertTrue(dao instanceof SneakersDAO);
    }

    @Test
    void shouldReturnHeelsDao() {
        ProductDAO<Heels> dao = DAOFactoryImpl.INSTANCE.getProductDAO(Heels.class);
        assertNotNull(dao);
        assertTrue(dao instanceof HeelsDAO);
    }
}
