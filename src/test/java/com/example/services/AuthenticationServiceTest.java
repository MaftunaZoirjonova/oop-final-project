package com.example.services;

import org.example.dal.dao.impl.DAOFactoryImpl;
import org.example.dal.search.criterias.UsersSearchCriteria;
import org.example.dal.search.parametrs.UsernameParameter;
import org.example.entities.users.User;
import org.example.exceptions.AuthenticationException;
import org.example.services.AuthenticationService;
import org.example.services.AuthenticationServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AuthenticationServiceTest {
    @Test
    public void shouldAuthenticateUser() {
        AuthenticationService<User> authenticationService = new AuthenticationServiceImpl<>(DAOFactoryImpl.INSTANCE);
        Assertions.assertDoesNotThrow(() -> authenticationService.authenticateUser(new UsersSearchCriteria().add(new UsernameParameter("admin")), "password"));
        Assertions.assertNotNull(authenticationService.getAuthenticatedUser());
    }

    @Test
    public void shouldNotAuthenticateUser() {
        AuthenticationService<User> authenticationService = new AuthenticationServiceImpl<>(DAOFactoryImpl.INSTANCE);
        Assertions.assertThrows(AuthenticationException.class, () -> authenticationService.authenticateUser(new UsersSearchCriteria().add(new UsernameParameter("admin")), "incorrect"));
        Assertions.assertNull(authenticationService.getAuthenticatedUser());
    }
}
