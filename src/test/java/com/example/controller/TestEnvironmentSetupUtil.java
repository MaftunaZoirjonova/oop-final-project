package com.example.controller;

import org.example.controllers.concrete.ConcreteController;
import org.example.controllers.io.IOHelper;
import org.example.dal.dao.DAOFactory;
import org.example.dal.dao.ProductDAO;
import org.example.dal.dao.UsersDAO;
import org.example.entities.Product;
import org.example.entities.users.User;
import org.example.services.AuthenticationService;
import org.example.services.ProductService;
import org.example.services.ProductServiceImpl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TestEnvironmentSetupUtil {
    private final Set<TestDescriptorProductEntry<?>> productEntries = new HashSet<>();
    private final Set<TestDescriptorUserEntry<?>> userEntries = new HashSet<>();

    public <A extends Product> TestEnvironmentSetupUtil addProduct(Class<A> type, ProductDAO<A> dao,
                                                                          Function<ProductService, ConcreteController<A>> controller) {
        this.productEntries.add(new TestDescriptorProductEntry<>(type, dao, controller));
        return this;
    }

    public <A extends User> TestEnvironmentSetupUtil addUser(Class<A> type, UsersDAO<A> dao, Function<AuthenticationService<A>, ConcreteController<A>> controller) {
        this.userEntries.add(new TestDescriptorUserEntry<>(type, dao, controller));
        return this;
    }

    public TestEnvironment setUp() {
        var out = new ByteArrayOutputStream();
        var err = new ByteArrayOutputStream();
        var daoFactory = new DAOFactory() {

            @Override
            @SuppressWarnings("unchecked")
            public <A extends Product> ProductDAO<A> getProductDAO(Class<A> applianceClass) {
                return ((ProductDAO<A>) productEntries.stream()
                        .filter(entry -> entry.type.equals(applianceClass))
                        .findFirst()
                        .orElseThrow()
                        .dao());
            }

            @Override
            public <A extends User> UsersDAO<A> getUserDAO(Class<A> userClass) {
                return ((UsersDAO<A>) userEntries.stream()
                        .filter(entry -> entry.type.equals(userClass))
                        .findFirst()
                        .orElseThrow()
                        .dao());
            }
        };
        var service = new ProductServiceImpl(daoFactory);
        var controllers = productEntries.stream()
                .collect(Collectors.<TestDescriptorProductEntry<?>, Class<?>, ConcreteController<?>>toMap(
                        TestDescriptorProductEntry::type,
                        entry -> entry.controller().apply(service)
                ));
        return new TestEnvironment(out, err, service, controllers);
    }


    public static record TestEnvironment(ByteArrayOutputStream out, ByteArrayOutputStream err,
                                         ProductService productService,
                                         Map<Class<?>, ConcreteController<?>> controllers) {

        @SuppressWarnings("unchecked")
        public <A> ConcreteController<A> getController(Class<A> type) {
            return ((ConcreteController<A>) controllers.get(type));
        }

        public IOHelper createIOHolder(String... input) {
            return new IOHelper(new ByteArrayInputStream(String.join(System.lineSeparator(), input)
                    .getBytes(StandardCharsets.UTF_8)), out, err);
        }
    }


    private static record TestDescriptorProductEntry<A extends Product>(
            Class<A> type, ProductDAO<A> dao, Function<ProductService, ConcreteController<A>> controller) {

        @Override
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            return obj instanceof TestDescriptorProductEntry<?> entry && entry.type.equals(this.type);
        }

        @Override
        public int hashCode() {
            return type.hashCode();
        }
    }

    private static record TestDescriptorUserEntry<A extends User>(
            Class<A> type, UsersDAO<A> dao, Function<AuthenticationService<A>, ConcreteController<A>> controller) {

        @Override
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            return obj instanceof TestDescriptorUserEntry<?> entry && entry.type.equals(this.type);
        }

        @Override
        public int hashCode() {
            return type.hashCode();
        }
    }
}
