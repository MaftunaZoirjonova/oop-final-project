package com.example.controller;

import org.example.config.RawConverters;
import org.example.controllers.DispatcherController;
import org.example.controllers.concrete.*;
import org.example.dal.dao.impl.*;
import org.example.entities.clothing.Dress;
import org.example.entities.clothing.Pants;
import org.example.entities.clothing.Shirt;
import org.example.entities.footwear.Heels;
import org.example.entities.footwear.Shoes;
import org.example.entities.footwear.Sneakers;
import org.example.entities.users.User;
import org.example.services.ProductServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DispatcherControllerTest {
    private TestEnvironmentSetupUtil.TestEnvironment env;
    private Map<String, ConcreteController<?>> controllers;

    @BeforeEach
    void setUp() {
        env = new TestEnvironmentSetupUtil()
                .addProduct(Shirt.class, new ShirtDAO("shirts.csv"), service -> new ShirtController(service, List.of(
                        RawConverters.NAME.generic(),
                        RawConverters.PRICE.generic(),
                        RawConverters.CLOTHING_SIZE.generic(),
                        RawConverters.CATEGORY.generic(),
                        RawConverters.MATERIAL.generic(),
                        RawConverters.COUNT.generic(),
                        RawConverters.COLOR.generic()
                )))
                .addProduct(Dress.class, new DressDAO("dresses.csv"), service -> new DressController(service, List.of(
                        RawConverters.NAME.generic(),
                        RawConverters.PRICE.generic(),
                        RawConverters.CLOTHING_SIZE.generic(),
                        RawConverters.CATEGORY.generic(),
                        RawConverters.STYLE.generic(),
                        RawConverters.COUNT.generic(),
                        RawConverters.COLOR.generic()
                )))
                .addProduct(Pants.class, new PantsDAO("pants.csv"), service -> new PantsController(service, List.of(
                        RawConverters.NAME.generic(),
                        RawConverters.PRICE.generic(),
                        RawConverters.CLOTHING_SIZE.generic(),
                        RawConverters.CATEGORY.generic(),
                        RawConverters.PANTS_LENGTHS.generic(),
                        RawConverters.COUNT.generic(),
                        RawConverters.COLOR.generic()
                )))
                .addProduct(Sneakers.class, new SneakersDAO("sneakers.csv"), service -> new SneakersController(service, List.of(
                        RawConverters.NAME.generic(),
                        RawConverters.PRICE.generic(),
                        RawConverters.CATEGORY.generic(),
                        RawConverters.COUNT.generic(),
                        RawConverters.BRAND.generic(),
                        RawConverters.FOOT_SIZE.generic()
                )))
                .addProduct(Heels.class, new HeelsDAO("heels.csv"), service -> new HeelsController(service, List.of(
                        RawConverters.NAME.generic(),
                        RawConverters.PRICE.generic(),
                        RawConverters.CATEGORY.generic(),
                        RawConverters.COUNT.generic(),
                        RawConverters.BRAND.generic(),
                        RawConverters.FOOT_SIZE.generic()
                )))
                .addProduct(Shoes.class, new ShoesDAO("pants.csv"), service -> new ShoesController(service, List.of(
                        RawConverters.NAME.generic(),
                        RawConverters.PRICE.generic(),
                        RawConverters.CATEGORY.generic(),
                        RawConverters.COUNT.generic(),
                        RawConverters.BRAND.generic(),
                        RawConverters.FOOT_SIZE.generic()
                )))
                .addUser(User.class, new UsersDAOImpl("users.csv"), authenticationService -> new AdminController(new ProductServiceImpl(DAOFactoryImpl.INSTANCE), authenticationService, List.of(
                        RawConverters.USERNAME.generic(),
                        RawConverters.ID.generic()
                )))
                .setUp();
        controllers = Map.of(
                "shirts", env.getController(Shirt.class),
                "dress", env.getController(Dress.class),
                "pants", env.getController(Pants.class),
                "sneakers", env.getController(Sneakers.class),
                "heels", env.getController(Heels.class),
                "shoes", env.getController(Shoes.class)
//                "admin", env.getController(User.class)
        );
    }

    @Test
    void shouldSearchSuccessfully() throws Exception {
        var dispatcher = dispatcher(
                "dress",
                "all",
                "quit"
        );
        dispatcher.listen();
        String expectedOut = """
                Dress [id = 1, name = Floral Maxi Dress, price = 89, count = 10, color = Pink, size = M, style = Bohemian]
                Dress [id = 2, name = Little Black Dress, price = 59, count = 15, color = Black, size = S, style = Classic]
                Dress [id = 3, name = Sequin Cocktail Dress, price = 129, count = 5, color = Silver, size = L, style = Glamorous]
                Dress [id = 4, name = Striped Shirt Dress, price = 49, count = 20, color = Blue and White, size = XS, style = Casual]
                Dress [id = 5, name = Lace Wedding Dress, price = 999, count = 3, color = Ivory, size = L, style = Romantic]
                Please, type what kind of product do you want to find (shirts, dress, pants, sneakers, heels, shoes):\s""";
//        assertTrue(env.out().toString().contains(expectedOut));
        assertEquals(expectedOut, env.out().toString().substring(439));
        assertEquals("", env.err().toString());
    }

    private DispatcherController dispatcher(String... input) {
        return new DispatcherController(env.createIOHolder(input), controllers);
    }
}
