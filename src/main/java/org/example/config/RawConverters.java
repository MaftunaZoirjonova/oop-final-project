package org.example.config;

import org.example.controllers.converters.ParameterConverter;
import org.example.controllers.converters.impl.*;
import org.example.entities.Product;

@SuppressWarnings("rawtypes")
public enum RawConverters {
    NAME(new NameParameterConverter()),
    CATEGORY(new CategoryParameterConverter()),
    COLOR(new ColorParameterConverter()),
    COUNT(new CountParameterConverter()),
    PRICE(new PriceParameterConverter()),
    CLOTHING_SIZE(new ClothingSizeParameterConverter()),
    STYLE(new DressStyleParameterConverter()),
    HEEL_HEIGHT(new HeelHeightParameterConverter()),
    PANTS_LENGTHS(new PantsLengthParameterConverter()),
    MATERIAL(new ShirtMaterialParameterConverter()),
    TOE_BOX(new ShoesToeBoxParameterConverter()),
    BRAND(new BrandParameterConverter()),
    FOOT_SIZE(new FootSizeParameterConverter()),
    SNEAKERS_MODEL(new SneakersModelParameterConverter()),
    ID(new IdParameterConverter()),
    USERNAME(new UsernameParameterConverter());

    private final ParameterConverter converter;

    RawConverters(ParameterConverter converter) {
        this.converter = converter;
    }

    @SuppressWarnings("unchecked")
    public <A> ParameterConverter<A> generic() {
        return (ParameterConverter<A>) converter;
    }
}
