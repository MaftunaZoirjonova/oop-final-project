package org.example.dal.search.criterias;

import org.example.dal.search.AbstractCriteria;
import org.example.entities.footwear.Sneakers;

public class SneakersSearchCriteria extends AbstractCriteria<Sneakers> {
    @Override
    public Class<Sneakers> getApplianceType() {
        return Sneakers.class;
    }
}
