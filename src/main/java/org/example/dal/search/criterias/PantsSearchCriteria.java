package org.example.dal.search.criterias;

import org.example.dal.search.AbstractCriteria;
import org.example.entities.clothing.Pants;

public class PantsSearchCriteria extends AbstractCriteria<Pants> {
    @Override
    public Class<Pants> getApplianceType() {
        return Pants.class;
    }
}
