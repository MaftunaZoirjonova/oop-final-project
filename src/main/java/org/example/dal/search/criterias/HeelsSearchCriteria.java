package org.example.dal.search.criterias;

import org.example.dal.search.AbstractCriteria;
import org.example.entities.footwear.Heels;

public class HeelsSearchCriteria extends AbstractCriteria<Heels> {
    @Override
    public Class<Heels> getApplianceType() {
        return Heels.class;
    }
}
