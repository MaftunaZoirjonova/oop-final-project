package org.example.dal.search.criterias;

import org.example.dal.search.AbstractCriteria;
import org.example.entities.clothing.Dress;

public class DressSearchCriteria extends AbstractCriteria<Dress> {
    @Override
    public Class<Dress> getApplianceType() {
        return Dress.class;
    }
}
