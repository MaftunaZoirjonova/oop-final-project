package org.example.dal.search.criterias;

import org.example.dal.search.AbstractCriteria;
import org.example.entities.users.User;

public class UsersSearchCriteria extends AbstractCriteria<User> {

    @Override
    public Class<User> getApplianceType() {
        return User.class;
    }
}
