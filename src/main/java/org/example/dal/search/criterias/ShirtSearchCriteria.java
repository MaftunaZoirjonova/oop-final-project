package org.example.dal.search.criterias;

import org.example.dal.search.AbstractCriteria;
import org.example.entities.clothing.Shirt;

public class ShirtSearchCriteria extends AbstractCriteria<Shirt> {
    @Override
    public Class<Shirt> getApplianceType() {
        return Shirt.class;
    }
}
