package org.example.dal.search.criterias;

import org.example.dal.search.AbstractCriteria;
import org.example.entities.footwear.Shoes;

public class ShoesSearchCriteria extends AbstractCriteria<Shoes> {
    @Override
    public Class<Shoes> getApplianceType() {
        return Shoes.class;
    }
}
