package org.example.dal.search;

import org.example.entities.Product;

public interface Parameter <A> {
    boolean test(A object);
    static <A extends Product> Parameter<A> any() {
        return appliance -> true;
    }

    static <A extends Product> Parameter<A> none() {
        return appliance -> false;
    }
}
