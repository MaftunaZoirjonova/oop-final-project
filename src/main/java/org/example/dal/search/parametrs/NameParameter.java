package org.example.dal.search.parametrs;

import org.example.dal.search.Parameter;
import org.example.entities.Product;

public record NameParameter(String name) implements Parameter<Product> {
    public NameParameter {
        if (name == null || name.isBlank()) {
            throw new InvalidParameterArguments("Name can't be empty");
        }
    }

    @Override
    public boolean test(Product product) {
        return product.getName().toLowerCase().contains(name.toLowerCase());
    }
}
