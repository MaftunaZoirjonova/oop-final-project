package org.example.dal.search.parametrs;

import org.example.dal.search.Parameter;
import org.example.entities.Product;

public record CategoryParameter(String category) implements Parameter<Product> {
    public CategoryParameter {
        if (category == null || category.isBlank()) {
            throw new InvalidParameterArguments("Category can't be empty");
        }
    }

    @Override
    public boolean test(Product product) {
        return product.getCategory().toLowerCase().contains(category.toLowerCase());
    }
}
