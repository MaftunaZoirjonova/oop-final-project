package org.example.dal.search.parametrs;

import org.example.dal.search.Parameter;
import org.example.entities.Product;

public record PriceParameter(Range<Long> range) implements Parameter<Product> {
    public PriceParameter {
        if (range.from() < 0.0) {
            throw new InvalidParameterArguments("Price can't be less than 0, but was " + range.from());
        }
    }

    @Override
    public boolean test(Product product) {
        return range.isIn(product.getPrice());
    }
}
