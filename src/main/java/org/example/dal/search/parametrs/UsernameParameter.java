package org.example.dal.search.parametrs;

import org.example.dal.search.Parameter;
import org.example.entities.users.User;

public record UsernameParameter(String username) implements Parameter<User> {
    public UsernameParameter {
        if (username == null || username.isBlank()) {
            throw new InvalidParameterArguments("Username can't be empty");
        }
    }

    @Override
    public boolean test(User user) {
        return user.getUsername().equals(username);
    }
}
