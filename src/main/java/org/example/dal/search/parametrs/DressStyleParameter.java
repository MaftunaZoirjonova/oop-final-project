package org.example.dal.search.parametrs;

import org.example.dal.search.Parameter;
import org.example.entities.clothing.Dress;

public record DressStyleParameter(String style) implements Parameter<Dress> {
    public DressStyleParameter {
        if (style == null || style.isBlank()) {
            throw new InvalidParameterArguments("Style can't be empty");
        }
    }

    @Override
    public boolean test(Dress dress) {
        return dress.getStyle().equalsIgnoreCase(style);
    }
}
