package org.example.dal.search.parametrs;

import org.example.dal.search.Parameter;
import org.example.entities.clothing.Clothing;

public record ColorParameter(String color) implements Parameter<Clothing> {
    public ColorParameter {
        if (color == null || color.isBlank()) {
            throw new InvalidParameterArguments("Color can't be empty");
        }
    }

    @Override
    public boolean test(Clothing clothing) {
        return clothing.getColor().equalsIgnoreCase(color);
    }
}
