package org.example.dal.search.parametrs;

import org.example.dal.search.Parameter;
import org.example.entities.footwear.FootWear;

public record FootSizeParameter(double size) implements Parameter<FootWear> {
    public FootSizeParameter {
        if (size == 0 ) {
            throw new InvalidParameterArguments("Size can't be 0");
        }
    }

    @Override
    public boolean test(FootWear product) {
        return product.getSize() == size;
    }
}
