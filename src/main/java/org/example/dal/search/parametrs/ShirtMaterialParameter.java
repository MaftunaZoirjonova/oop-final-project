package org.example.dal.search.parametrs;

import org.example.dal.search.Parameter;
import org.example.entities.clothing.Shirt;

public record ShirtMaterialParameter(String material) implements Parameter<Shirt> {
    public ShirtMaterialParameter {
        if (material == null || material.isBlank()) {
            throw new InvalidParameterArguments("Material can't be empty");
        }
    }

    @Override
    public boolean test(Shirt shirt) {
        return shirt.getMaterial().equalsIgnoreCase(material);
    }
}
