package org.example.dal.search.parametrs;

import org.example.dal.search.Parameter;
import org.example.entities.footwear.Sneakers;

public record SneakersModelParameter(String model) implements Parameter<Sneakers> {
    public SneakersModelParameter {
        if (model == null || model.isBlank()) {
            throw new InvalidParameterArguments("Model can't be empty");
        }
    }

    @Override
    public boolean test(Sneakers sneakers) {
        return sneakers.getBrand().toLowerCase().contains(model.toLowerCase());
    }
}
