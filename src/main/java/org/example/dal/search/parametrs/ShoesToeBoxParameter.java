package org.example.dal.search.parametrs;

import org.example.dal.search.Parameter;
import org.example.entities.footwear.Shoes;

public record ShoesToeBoxParameter(Shoes.ToeBox toeBox) implements Parameter<Shoes> {
    public ShoesToeBoxParameter {
        if (toeBox == null) {
            throw new InvalidParameterArguments("Size can't be empty");
        }
    }

    @Override
    public boolean test(Shoes shoes) {
        return shoes.getToeBox().equals(toeBox);
    }
}
