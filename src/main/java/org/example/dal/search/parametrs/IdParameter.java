package org.example.dal.search.parametrs;

import org.example.dal.search.Parameter;
import org.example.entities.Product;

public record IdParameter(int id) implements Parameter<Product> {
    public IdParameter{
        if (id <= 0) {
            throw new InvalidParameterArguments("Id can't be below zero");
        }
    }

    @Override
    public boolean test(Product object) {
        return object.getId() == id;
    }
}
