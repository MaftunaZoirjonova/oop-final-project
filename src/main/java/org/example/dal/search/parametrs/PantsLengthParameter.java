package org.example.dal.search.parametrs;

import org.example.dal.search.Parameter;
import org.example.entities.clothing.Pants;

public record PantsLengthParameter(int length) implements Parameter<Pants> {
    public PantsLengthParameter {
        if (length == 0) {
            throw new InvalidParameterArguments("Length can't be 0");
        }
    }

    @Override
    public boolean test(Pants pants) {
        return pants.getLength() == length;
    }
}
