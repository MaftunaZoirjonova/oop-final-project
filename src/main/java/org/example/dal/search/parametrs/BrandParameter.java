package org.example.dal.search.parametrs;

import org.example.dal.search.Parameter;
import org.example.entities.footwear.FootWear;

public record BrandParameter(String brand) implements Parameter<FootWear> {
    public BrandParameter {
        if (brand == null || brand.isBlank()) {
            throw new InvalidParameterArguments("Brand can't be empty");
        }
    }

    @Override
    public boolean test(FootWear product) {
        return product.getBrand().equalsIgnoreCase(brand);
    }
}
