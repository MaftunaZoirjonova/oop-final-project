package org.example.dal.search.parametrs;

import org.example.dal.search.Parameter;
import org.example.entities.clothing.Clothing;

public record ClothingSizeParameter(Clothing.Size size) implements Parameter<Clothing> {
    public ClothingSizeParameter {
        if (size == null) {
            throw new InvalidParameterArguments("Size can't be empty");
        }
    }

    @Override
    public boolean test(Clothing clothing) {
        return clothing.getSize().equals(size);
    }
}
