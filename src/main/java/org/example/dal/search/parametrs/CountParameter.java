package org.example.dal.search.parametrs;

import org.example.dal.search.Parameter;
import org.example.entities.Product;

public record CountParameter(String operator, int count) implements Parameter<Product> {
    public CountParameter {
        if (operator == null || operator.isBlank()) {
            throw new InvalidParameterArguments("Operator is required");
        }
    }

    @Override
    public boolean test(Product product) {
        if (operator.equals(">")) {
            return product.getCount() > count;
        }
        if (operator.equals("<")) {
            return product.getCount() < count;
        }
        if (operator.equals(">=")) {
            return product.getCount() >= count;
        }
        if (operator.equals("<=")) {
            return product.getCount() <= count;
        }
        if (operator.equals("=")) {
            return product.getCount() == count;
        }
        throw new InvalidParameterArguments("Undefined operator: " + operator);
    }
}
