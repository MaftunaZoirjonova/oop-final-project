package org.example.dal.search.parametrs;

import org.example.dal.search.Parameter;
import org.example.entities.footwear.Heels;

public record HeelHeightParameter(int heelHeight) implements Parameter<Heels> {
    public HeelHeightParameter {
        if (heelHeight == 0) {
            throw new InvalidParameterArguments("Heel height can't be 0");
        }
    }

    @Override
    public boolean test(Heels heels) {
        return heels.getHeelHeight() == heelHeight;
    }
}
