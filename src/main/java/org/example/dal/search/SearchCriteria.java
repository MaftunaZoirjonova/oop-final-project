package org.example.dal.search;

import java.util.Map;

public interface SearchCriteria<A> {
    Class<A> getApplianceType();
    <P extends Parameter<A>> SearchCriteria<A> add(P parameter);
    boolean test(A product);
    Map<Class<?>, Parameter<A>> getParameters();
}
