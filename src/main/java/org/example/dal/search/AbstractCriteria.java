package org.example.dal.search;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractCriteria<A> implements SearchCriteria<A> {
    protected final Map<Class<?>, Parameter<A>> parameters = new HashMap<>();

    @Override
    public <F extends Parameter<A>> SearchCriteria<A> add(F parameter) {
        parameters.put(parameter.getClass(), parameter);
        return this;
    }

    @Override
    public boolean test(A object) {
        for (Map.Entry<Class<?>, Parameter<A>> set : parameters.entrySet()) {
            boolean testResult = set.getValue().test(object);
            if (!testResult) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Map<Class<?>, Parameter<A>> getParameters() {
        return parameters;
    }
}
