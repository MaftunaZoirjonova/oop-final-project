package org.example.dal.writer.impl;

import org.example.dal.writer.CsvLineWriter;
import org.example.entities.Product;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractProductCsvLineWriter<T extends Product> implements CsvLineWriter<T> {
    protected List<String> writeProductFields(T entity) {
        List<String> csvLine = new ArrayList<String>();
        int id = entity.getId();
        csvLine.add(String.valueOf(entity.getId()));
        csvLine.add(entity.getName());
        csvLine.add(String.valueOf(entity.getPrice()));
        csvLine.add(entity.getCategory());
        csvLine.add(String.valueOf(entity.getCount()));

        return csvLine;
    }
}
