package org.example.dal.writer.impl;

import org.example.entities.clothing.Clothing;

import java.util.List;

public abstract class AbstractClothingCsvLineWriter<T extends Clothing> extends AbstractProductCsvLineWriter<T> {
    @Override
    protected List<String> writeProductFields(T entity) {
        List<String> csvLine = super.writeProductFields(entity);
        csvLine.add(entity.getColor());
        csvLine.add(String.valueOf(entity.getSize()));

        return csvLine;
    }
}
