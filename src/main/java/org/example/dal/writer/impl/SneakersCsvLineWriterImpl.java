package org.example.dal.writer.impl;

import org.example.dal.parser.impl.AbstractFootWearCsvLineParser;
import org.example.entities.footwear.Sneakers;

import java.util.List;

public class SneakersCsvLineWriterImpl extends AbstractFootWearCsvLineWriter<Sneakers> {
    @Override
    public List<String> write(Sneakers entity) {
        List<String> csvLine = writeProductFields(entity);
        csvLine.add(entity.getModel());

        return csvLine;
    }
}
