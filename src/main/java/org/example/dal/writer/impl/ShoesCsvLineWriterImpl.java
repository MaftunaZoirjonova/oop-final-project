package org.example.dal.writer.impl;

import org.example.dal.parser.impl.AbstractFootWearCsvLineParser;
import org.example.entities.footwear.Shoes;

import java.util.List;

public class ShoesCsvLineWriterImpl extends AbstractFootWearCsvLineWriter<Shoes> {
    @Override
    public List<String> write(Shoes entity) {
        List<String> csvLine = writeProductFields(entity);
        csvLine.add(String.valueOf(entity.getToeBox()));

        return csvLine;
    }
}
