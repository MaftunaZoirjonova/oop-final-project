package org.example.dal.writer.impl;

import org.example.dal.parser.impl.AbstractClothingCsvLineParser;
import org.example.entities.Product;
import org.example.entities.clothing.Dress;

import java.util.List;

public class DressCsvLineWriterImpl extends AbstractClothingCsvLineWriter<Dress> {

    @Override
    public List<String> write(Dress entity) {
        List<String> csvLine = writeProductFields(entity);
        csvLine.add(entity.getStyle());

        return csvLine;
    }
}
