package org.example.dal.writer.impl;

import org.example.dal.parser.impl.AbstractClothingCsvLineParser;
import org.example.entities.clothing.Pants;

import java.util.List;

public class PantsCsvLineWriterImpl extends AbstractClothingCsvLineWriter<Pants> {
    @Override
    public List<String> write(Pants entity) {
        List<String> csvLine = writeProductFields(entity);
        csvLine.add(String.valueOf(entity.getLength()));

        return csvLine;
    }
}
