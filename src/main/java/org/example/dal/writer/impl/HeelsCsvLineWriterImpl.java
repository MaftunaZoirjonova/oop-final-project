package org.example.dal.writer.impl;

import org.example.dal.parser.impl.AbstractFootWearCsvLineParser;
import org.example.entities.footwear.Heels;

import java.util.List;

public class HeelsCsvLineWriterImpl extends AbstractFootWearCsvLineWriter<Heels> {
    @Override
    public List<String> write(Heels entity) {
        List<String> csvLine = writeProductFields(entity);
        csvLine.add(String.valueOf(entity.getHeelHeight()));

        return csvLine;
    }
}
