package org.example.dal.writer.impl;

import org.example.dal.parser.impl.AbstractClothingCsvLineParser;
import org.example.entities.clothing.Shirt;

import java.util.List;

public class ShirtCsvLineWriterImpl extends AbstractClothingCsvLineWriter<Shirt> {
    @Override
    public List<String> write(Shirt entity) {
        List<String> csvLine = writeProductFields(entity);
        csvLine.add(entity.getMaterial());

        return csvLine;
    }
}
