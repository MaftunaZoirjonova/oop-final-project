package org.example.dal.writer.impl;

import org.example.entities.footwear.FootWear;

import java.util.List;

public abstract class AbstractFootWearCsvLineWriter<T extends FootWear> extends AbstractProductCsvLineWriter<T> {
    @Override
    protected List<String> writeProductFields(T entity) {
        List<String> csvLine =  super.writeProductFields(entity);
        csvLine.add(entity.getBrand());
        csvLine.add(String.valueOf(entity.getSize()));

        return csvLine;
    }
}
