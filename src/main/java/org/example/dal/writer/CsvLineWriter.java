package org.example.dal.writer;

import org.example.entities.Product;

import java.util.List;

public interface CsvLineWriter<T extends Product> {
    public List<String> write(T entity);
}
