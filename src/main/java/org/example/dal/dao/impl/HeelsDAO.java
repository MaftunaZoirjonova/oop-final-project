package org.example.dal.dao.impl;

import org.example.dal.dao.AbstractProductDAO;
import org.example.dal.parser.impl.HeelsCsvLineParserImpl;
import org.example.dal.writer.impl.HeelsCsvLineWriterImpl;
import org.example.entities.footwear.Heels;

public class HeelsDAO extends AbstractProductDAO<Heels> {
    public HeelsDAO(String path) {
        super(path, new HeelsCsvLineParserImpl(), new HeelsCsvLineWriterImpl());
    }
}
