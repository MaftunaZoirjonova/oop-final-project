package org.example.dal.dao.impl;

import org.example.dal.dao.AbstractProductDAO;
import org.example.dal.parser.impl.PantsCsvLineParserImpl;
import org.example.dal.writer.impl.PantsCsvLineWriterImpl;
import org.example.entities.clothing.Pants;

public class PantsDAO extends AbstractProductDAO<Pants> {
    public PantsDAO(String path) {
        super(path, new PantsCsvLineParserImpl(), new PantsCsvLineWriterImpl());
    }
}
