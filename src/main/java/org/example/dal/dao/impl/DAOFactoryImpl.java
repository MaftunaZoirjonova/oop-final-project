package org.example.dal.dao.impl;

import org.example.dal.dao.DAOFactory;
import org.example.dal.dao.ProductDAO;
import org.example.dal.dao.UsersDAO;
import org.example.entities.Product;
import org.example.entities.clothing.Dress;
import org.example.entities.clothing.Pants;
import org.example.entities.clothing.Shirt;
import org.example.entities.footwear.Heels;
import org.example.entities.footwear.Shoes;
import org.example.entities.footwear.Sneakers;
import org.example.entities.users.User;

public enum DAOFactoryImpl implements DAOFactory {
    INSTANCE;

    @SuppressWarnings("unchecked")
    public <A extends Product> ProductDAO<A> getProductDAO(Class<A> productClass) {
        if (Dress.class.equals(productClass)) {
            return (ProductDAO<A>) new DressDAO("dresses.csv");
        }
        if (Heels.class.equals(productClass)) {
            return (ProductDAO<A>) new HeelsDAO("heels.csv");
        }
        if (Pants.class.equals(productClass)) {
            return (ProductDAO<A>) new PantsDAO("pants.csv");
        }
        if (Shirt.class.equals(productClass)) {
            return (ProductDAO<A>) new ShirtDAO("shirts.csv");
        }
        if (Shoes.class.equals(productClass)) {
            return (ProductDAO<A>) new ShoesDAO("shoes.csv");
        }
        if (Sneakers.class.equals(productClass)) {
            return (ProductDAO<A>) new SneakersDAO("sneakers.csv");
        }
        return null;
    }

    @Override
    public <T extends User> UsersDAO<T> getUserDAO(Class<T> userClass) {
        if (User.class.equals(userClass)) {
            return (UsersDAO<T>) new UsersDAOImpl("users.csv");
        }
        return null;
    }
}
