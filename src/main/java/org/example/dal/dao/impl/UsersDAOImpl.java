package org.example.dal.dao.impl;

import org.example.dal.dao.AbstractUsersDAO;
import org.example.dal.parser.impl.UsersCsvLineParserImpl;
import org.example.entities.users.User;

public class UsersDAOImpl extends AbstractUsersDAO<User> {
    public UsersDAOImpl(String path) {
        super(path, new UsersCsvLineParserImpl());
    }
}
