package org.example.dal.dao.impl;

import org.example.dal.dao.AbstractProductDAO;
import org.example.dal.parser.impl.SneakersCsvLineParserImpl;
import org.example.dal.writer.impl.SneakersCsvLineWriterImpl;
import org.example.entities.footwear.Sneakers;

public class SneakersDAO extends AbstractProductDAO<Sneakers> {
    public SneakersDAO(String path) {
        super(path, new SneakersCsvLineParserImpl(), new SneakersCsvLineWriterImpl());
    }
}
