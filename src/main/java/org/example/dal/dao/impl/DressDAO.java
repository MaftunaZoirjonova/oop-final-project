package org.example.dal.dao.impl;

import org.example.dal.dao.AbstractProductDAO;
import org.example.dal.parser.impl.DressCsvLineParserImpl;
import org.example.dal.writer.impl.DressCsvLineWriterImpl;
import org.example.entities.clothing.Dress;

public class DressDAO extends AbstractProductDAO<Dress> {
    public DressDAO(String path) {
        super(path, new DressCsvLineParserImpl(), new DressCsvLineWriterImpl());
    }
}
