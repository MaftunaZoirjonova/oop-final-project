package org.example.dal.dao.impl;

import org.example.dal.dao.AbstractProductDAO;
import org.example.dal.parser.impl.ShirtCsvLineParserImpl;
import org.example.dal.writer.impl.ShirtCsvLineWriterImpl;
import org.example.entities.clothing.Shirt;

public class ShirtDAO extends AbstractProductDAO<Shirt> {
    public ShirtDAO(String path) {
        super(path, new ShirtCsvLineParserImpl(), new ShirtCsvLineWriterImpl());
    }
}
