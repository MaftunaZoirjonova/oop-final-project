package org.example.dal.dao.impl;

import org.example.dal.dao.AbstractProductDAO;
import org.example.dal.parser.impl.ShoesCsvLineParserImpl;
import org.example.dal.writer.impl.ShoesCsvLineWriterImpl;
import org.example.entities.footwear.Shoes;

public class ShoesDAO extends AbstractProductDAO<Shoes> {
    public ShoesDAO(String path) {
        super(path, new ShoesCsvLineParserImpl(), new ShoesCsvLineWriterImpl());
    }
}
