package org.example.dal.dao;

import org.example.dal.search.SearchCriteria;
import org.example.entities.Product;

import java.util.Collection;

public interface ProductDAO<A extends Product> {
    Collection<A> find(SearchCriteria<A> criteria);
    A create(A item);
    void delete(int id);
}
