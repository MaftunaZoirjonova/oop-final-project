package org.example.dal.dao;

import org.example.entities.Product;
import org.example.entities.users.User;

public interface DAOFactory {
    public <A extends Product> ProductDAO<A> getProductDAO(Class<A> productClass);
    public <A extends User> UsersDAO<A> getUserDAO(Class<A> userClass);
}
