package org.example.dal.dao;

import org.example.dal.parser.CsvLineParser;
import org.example.dal.search.SearchCriteria;
import org.example.dal.writer.CsvLineWriter;
import org.example.entities.Product;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public abstract class AbstractProductDAO<A extends Product> implements ProductDAO<A> {
    private final Path csvFile;
    private final CsvLineParser<A> parser;
    private final CsvLineWriter<A> writer;

    protected AbstractProductDAO(String path, CsvLineParser<A> parser, CsvLineWriter<A> writer) {
        var resource = getClass().getClassLoader().getResource(path);
        if (resource == null) {
            throw new IllegalArgumentException("No CSV file is found: " + path);
        }
        try {
            this.csvFile = Path.of(resource.toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        this.parser = Objects.requireNonNull(parser);
        this.writer = Objects.requireNonNull(writer);
    }

    public Collection<A> find(SearchCriteria<A> criteria) {
        try {
            BufferedReader input = Files.newBufferedReader(this.csvFile);
            input.readLine();
            String line;
            ArrayList<A> products = new ArrayList<A>();
            while ((line = input.readLine()) != null) {
                String[] csvLine = line.split(",");
                A product = parser.parse(csvLine);
                if (criteria.test(product))
                    products.add(product);
            }
            return products;
        }
        catch (IOException e) {
            return List.of();
        }
    }

    @Override
    public A create(A item) {
        List<String> csvLine = writer.write(item);
        String[] csvLineArray = csvLine.toArray(new String[0]);
        try (BufferedWriter output = Files.newBufferedWriter(this.csvFile, StandardOpenOption.APPEND)) {
            output.write("\n");
            for (int i = 0; i < csvLineArray.length; i++) {
                output.write(csvLineArray[i]);
                if (i < csvLineArray.length - 1) {
                    output.write(",");
                }
            }
            return item;
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public void delete(int id) {
        String tempCsvPath = csvFile + ".tmp";
        try (BufferedReader input = Files.newBufferedReader(csvFile);
        BufferedWriter output = new BufferedWriter(new FileWriter(tempCsvPath))) {
            String header = input.readLine();
            output.write(header + System.getProperty("line.separator"));
            String line;
            boolean firstLine = true;
            while ((line = input.readLine()) != null) {
                String[] csvLine = line.split(",");
                A product = parser.parse(csvLine);
                if (product.getId() != id) {
                    if (!firstLine) {
                        output.newLine();
                    } else {
                        firstLine = false;
                    }
                    output.write(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        File originalFile = new File(csvFile.toString());
        File tempFile = new File(csvFile + ".tmp");
        tempFile.renameTo(originalFile);
    }

    public Path getCsvFile() {
        return csvFile;
    }
}
