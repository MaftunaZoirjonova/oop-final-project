package org.example.dal.dao;

import org.example.dal.search.SearchCriteria;
import org.example.entities.users.User;

public interface UsersDAO<A extends User> {
    A findOne(SearchCriteria<A> criteria);
}
