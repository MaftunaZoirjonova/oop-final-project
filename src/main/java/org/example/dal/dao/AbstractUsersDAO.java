package org.example.dal.dao;

import org.example.dal.parser.CsvLineParser;
import org.example.dal.search.SearchCriteria;
import org.example.entities.users.User;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public abstract class AbstractUsersDAO<A extends User> implements UsersDAO<A> {

    private final Path csvFile;
    private final CsvLineParser<A> parser;

    protected AbstractUsersDAO(String path, CsvLineParser<A> parser) {
        var resource = getClass().getClassLoader().getResource(path);
        if (resource == null) {
            throw new IllegalArgumentException("No CSV file is found: " + path);
        }
        try {
            this.csvFile = Path.of(resource.toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        this.parser = Objects.requireNonNull(parser);
    }

    @Override
    public A findOne(SearchCriteria<A> criteria) {
        try (BufferedReader input = Files.newBufferedReader(this.csvFile)) {
            input.readLine();
            String line;
            while ((line = input.readLine()) != null) {
                String[] csvLine = line.split(",");
                A user = parser.parse(csvLine);
                if (criteria.test(user))
                    return user;
            }
            return null;

        } catch (IOException e) {
            return null;
        }

    }
}
