package org.example.dal.parser;

import org.example.dal.parser.impl.*;
import org.example.dal.search.parametrs.InvalidParameterArguments;
import org.example.entities.Product;

public enum CsvLineParserFactory {

    INSTANCE;

    public CsvLineParser getProductCsvLineParser(String productType) {
        return switch (productType) {
            case "dress" -> new DressCsvLineParserImpl();
            case "heels" -> new HeelsCsvLineParserImpl();
            case "pants" -> new PantsCsvLineParserImpl();
            case "shirt" -> new ShirtCsvLineParserImpl();
            case "shoes" -> new ShoesCsvLineParserImpl();
            case "sneakers" -> new SneakersCsvLineParserImpl();
            default -> throw new InvalidParameterArguments("There is no parser for " + productType + " product type");
        };
    }
}
