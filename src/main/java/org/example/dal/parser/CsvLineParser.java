package org.example.dal.parser;

import org.example.entities.Product;

public interface CsvLineParser<T> {
    public T parse(String[] csvLine);
}
