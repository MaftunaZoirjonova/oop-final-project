package org.example.dal.parser.impl;

import org.example.dal.parser.CsvLineParser;
import org.example.entities.Product;

public abstract class AbstractProductCsvLineParser<T extends Product> implements CsvLineParser<T> {
    protected T fillProductFields(T entity, String[] csvLine) {
        entity.setId(Integer.parseInt(csvLine[0]));
        entity.setName(csvLine[1]);
        entity.setPrice(Long.parseLong(csvLine[2]));
        entity.setCategory(csvLine[3]);
        entity.setCount(Integer.parseInt(csvLine[4]));
        return entity;
    }
}
