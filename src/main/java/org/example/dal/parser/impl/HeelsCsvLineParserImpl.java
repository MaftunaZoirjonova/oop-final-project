package org.example.dal.parser.impl;

import org.example.entities.footwear.Heels;

public class HeelsCsvLineParserImpl extends AbstractFootWearCsvLineParser<Heels> {
    @Override
    public Heels parse(String[] csvLine) {
        Heels heels = new Heels();
        heels = this.fillFootWearFields(heels, csvLine);
        heels.setHeelHeight(Integer.parseInt(csvLine[7]));
        return heels;
    }
}
