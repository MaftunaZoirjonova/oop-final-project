package org.example.dal.parser.impl;

import org.example.entities.footwear.Sneakers;

public class SneakersCsvLineParserImpl extends AbstractFootWearCsvLineParser<Sneakers> {
    @Override
    public Sneakers parse(String[] csvLine) {
        Sneakers sneakers = new Sneakers();
        sneakers = this.fillFootWearFields(sneakers, csvLine);
        sneakers.setModel(csvLine[7]);
        return sneakers;
    }
}
