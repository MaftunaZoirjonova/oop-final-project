package org.example.dal.parser.impl;

import org.example.entities.clothing.Dress;

public class DressCsvLineParserImpl extends AbstractClothingCsvLineParser<Dress> {
    @Override
    public Dress parse(String[] csvLine) {
        Dress dress = new Dress();
        dress = this.fillClothingFields(dress, csvLine);
        dress.setStyle(csvLine[7]);
        return dress;
    }
}
