package org.example.dal.parser.impl;

import org.example.entities.clothing.Shirt;

public class ShirtCsvLineParserImpl extends AbstractClothingCsvLineParser<Shirt> {
    @Override
    public Shirt parse(String[] csvLine) {
        Shirt shirt = new Shirt();
        shirt = this.fillClothingFields(shirt, csvLine);
        shirt.setMaterial(csvLine[7]);
        return shirt;
    }
}
