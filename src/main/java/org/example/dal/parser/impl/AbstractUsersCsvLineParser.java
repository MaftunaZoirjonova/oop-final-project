package org.example.dal.parser.impl;

import org.example.dal.parser.CsvLineParser;
import org.example.entities.users.AdminRole;
import org.example.entities.users.ClientRole;
import org.example.entities.users.User;

public abstract class AbstractUsersCsvLineParser<T extends User> implements CsvLineParser<T> {
    protected T fillUsersFields(T entity, String[] csvLine) {
        entity.setId(Integer.parseInt(csvLine[0]));
        entity.setName(csvLine[1]);
        entity.setUsername(csvLine[2]);
        entity.setHashedPassword(csvLine[3]);
        boolean isAdmin = Boolean.parseBoolean(csvLine[4]);
        entity.addRole(isAdmin ? new AdminRole() : new ClientRole());

        return entity;
    }
}
