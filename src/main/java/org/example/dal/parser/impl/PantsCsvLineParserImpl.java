package org.example.dal.parser.impl;

import org.example.entities.clothing.Pants;

public class PantsCsvLineParserImpl extends AbstractClothingCsvLineParser<Pants> {
    @Override
    public Pants parse(String[] csvLine) {
        Pants pants = new Pants();
        pants = this.fillClothingFields(pants, csvLine);
        pants.setLength(Integer.parseInt(csvLine[7]));
        return pants;
    }
}
