package org.example.dal.parser.impl;

import org.example.entities.footwear.Shoes;

public class ShoesCsvLineParserImpl extends AbstractFootWearCsvLineParser<Shoes> {
    @Override
    public Shoes parse(String[] csvLine) {
        Shoes shoes = new Shoes();
        shoes = this.fillFootWearFields(shoes, csvLine);
        shoes.setToeBox(Shoes.ToeBox.valueOf(csvLine[7]));
        return shoes;
    }
}
