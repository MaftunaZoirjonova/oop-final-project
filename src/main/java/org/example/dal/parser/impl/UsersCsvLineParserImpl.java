package org.example.dal.parser.impl;

import org.example.entities.users.User;

public class UsersCsvLineParserImpl extends AbstractUsersCsvLineParser<User> {
    @Override
    public User parse(String[] csvLine) {
        User user = new User();
        this.fillUsersFields(user, csvLine);

        return user;
    }
}
