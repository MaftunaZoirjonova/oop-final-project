package org.example.dal.parser.impl;

import org.example.entities.footwear.FootWear;

public abstract class AbstractFootWearCsvLineParser<T extends FootWear> extends AbstractProductCsvLineParser<T> {
    protected T fillFootWearFields(T entity, String[] csvLine) {
        entity = this.fillProductFields(entity, csvLine);
        entity.setBrand(csvLine[5]);
        entity.setSize(Double.parseDouble(csvLine[6]));
        return entity;
    }
}
