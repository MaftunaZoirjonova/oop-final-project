package org.example.dal.parser.impl;

import org.example.dal.parser.CsvLineParser;
import org.example.entities.clothing.Clothing;

public abstract class AbstractClothingCsvLineParser<T extends Clothing> extends AbstractProductCsvLineParser<T> {
    protected T fillClothingFields(T entity, String[] csvLine) {
        entity = this.fillProductFields(entity, csvLine);
        entity.setColor(csvLine[5]);
        entity.setSize(Clothing.Size.valueOf(csvLine[6]));

        return entity;
    }
}
