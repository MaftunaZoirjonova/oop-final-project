package org.example;

import org.example.controllers.ControllerFactory;
import org.example.entities.users.User;
import org.example.services.AuthenticationService;
import org.example.services.AuthenticationServiceImpl;
import org.example.services.ProductService;
import org.example.services.ServiceFactory;

public class Main {
    public static void main(String[] args) {
        ProductService productService = ServiceFactory.INSTANCE.getProductService();
        AuthenticationService<User> authenticationService = ServiceFactory.INSTANCE.getAuthenticationService();
        try (var dispatcher = ControllerFactory.INSTANCE.dispatcherController(productService, authenticationService)) {
            dispatcher.listen();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}