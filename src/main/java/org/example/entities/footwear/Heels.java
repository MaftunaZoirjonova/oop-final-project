package org.example.entities.footwear;

public class Heels extends FootWear {
    private int heelHeight;

    public int getHeelHeight() {
        return heelHeight;
    }

    public void setHeelHeight(int heelHeight) {
        this.heelHeight = heelHeight;
    }

    @Override
    public String toString() {
        String baseString = super.toString();
        return "Heels " + baseString + ", heel height = " + getHeelHeight() + "]";
    }

    @Override
    public Class getApplianceType() {
        return Heels.class;
    }
}
