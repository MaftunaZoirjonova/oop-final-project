package org.example.entities.footwear;

import org.example.entities.Product;

public abstract class FootWear extends Product {
    private String brand;
    private double size;

    public String getBrand() {
        return brand;
    }

    public double getSize() {
        return size;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setSize(double size) {
        this.size = size;
    }

    @Override
    public String toString() {
        String baseString = super.toString();
        return baseString + ", brand = " + getBrand() + ", size = " + getSize();
    }
}
