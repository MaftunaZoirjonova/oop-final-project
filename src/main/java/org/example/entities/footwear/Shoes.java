package org.example.entities.footwear;

public class Shoes extends FootWear {

    private ToeBox toeBox;

    public ToeBox getToeBox() {
        return toeBox;
    }

    public void setToeBox(ToeBox toeBox) {
        this.toeBox = toeBox;
    }

    public enum ToeBox {
        Almond,
        Pointed,
        Round,
        Squared,
    }

    @Override
    public String toString() {
        String baseString = super.toString();
        return "Shoes " + baseString + ", toe box = " + getToeBox().toString() + "]";
    }

    @Override
    public Class getApplianceType() {
        return Shoes.class;
    }
}
