package org.example.entities.footwear;

public class Sneakers extends FootWear {
    private String model;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        String baseString = super.toString();
        return "Sneakers " + baseString + ", model = " + getModel() + "]";
    }

    @Override
    public Class getApplianceType() {
        return Sneakers.class;
    }
}
