package org.example.entities;

import java.util.Objects;

public abstract class Product {
    private int id;
    private String name;
    private Long price;
    private String category;
    private int count;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getPrice() {
        return price;
    }

    public String getCategory() {
        return category;
    }

    public int getCount() {
        return count;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (!(obj instanceof Product)) {
            return false;
        }
        Product other = (Product) obj;
        return Objects.equals(id, other.id);
    }

    @Override
    public String toString() {
        return "[id = " + id + ", name = " + name + ", price = " + (price / 100) + ", count = " + count;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public abstract Class getApplianceType();

    public void setCount(int count) {
        this.count = count;
    }
}
