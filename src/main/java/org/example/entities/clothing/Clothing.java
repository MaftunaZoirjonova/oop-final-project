package org.example.entities.clothing;

import org.example.entities.Product;

public abstract class Clothing extends Product {
    private String color;
    private Size size;

    public String getColor() {
        return color;
    }

    public Size getSize() {
        return size;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public enum Size {
        XS,
        S,
        M,
        L,
        XL,
        XXL,
    }

    @Override
    public String toString() {
        String baseString = super.toString();
        return baseString + ", color = " + getColor() + ", size = " + getSize().toString();
    }
}
