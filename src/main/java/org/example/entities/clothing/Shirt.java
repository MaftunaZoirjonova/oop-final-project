package org.example.entities.clothing;

public class Shirt extends Clothing {
    private String material;

    public void setMaterial(String material_) {
        material = material_;
    }

    public String getMaterial() {
        return material;
    }

    @Override
    public String toString() {
        String baseString = super.toString();
        return "Shirt " + baseString + ", material = " + getMaterial() + "]";
    }

    @Override
    public Class getApplianceType() {
        return Shirt.class;
    }
}
