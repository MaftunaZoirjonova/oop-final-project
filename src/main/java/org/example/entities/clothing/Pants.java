package org.example.entities.clothing;

public class Pants extends Clothing{
    private int length;

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public String toString() {
        String baseString = super.toString();
        return "Pants " + baseString + ", length = " + getLength() + "]";
    }

    @Override
    public Class getApplianceType() {
        return Pants.class;
    }
}
