package org.example.entities.clothing;

import org.example.entities.footwear.FootWear;

public class Dress extends Clothing {
    private String style;

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    @Override
    public String toString() {
        String baseString = super.toString();
        return "Dress " + baseString + ", style = " + getStyle() + "]";
    }

    @Override
    public Class getApplianceType() {
        return Dress.class;
    }
}
