package org.example.entities.users;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class User {
    private int id;
    private String name;
    private String username;
    private String hashedPassword;
    private final ArrayList<Role> roles;

    public User() {
        roles = new ArrayList<>();
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public void addRole(Role role) {
        if (hasRole(role)) {
            return;
        }
        roles.add(role);
    }

    public void removeRole(Role role) {
        if (!hasRole(role)) {
            return;
        }
        roles.remove(role);
    }

    public boolean hasRole(Role role) {
        List<Role> foundRoles = roles.stream().filter(r -> r.equals(role)).toList();
        return foundRoles.size() > 0;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
