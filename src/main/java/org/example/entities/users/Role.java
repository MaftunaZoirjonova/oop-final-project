package org.example.entities.users;

import java.util.Objects;

public abstract class Role {
    private String name;

    public static String ADMIN = "Admin";
    public static String CLIENT = "Client";

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (!(obj instanceof Role)) {
            return false;
        }
        Role other = (Role) obj;
        return Objects.equals(name, other.name);
    }
}
