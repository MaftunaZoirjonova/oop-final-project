package org.example.entities.users;

public class AdminRole extends Role {
    @Override
    public String getName() {
        return Role.ADMIN;
    }
}
