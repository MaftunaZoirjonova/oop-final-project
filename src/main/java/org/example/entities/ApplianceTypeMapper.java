package org.example.entities;

import org.example.entities.clothing.Dress;
import org.example.entities.clothing.Pants;
import org.example.entities.clothing.Shirt;
import org.example.entities.footwear.Heels;
import org.example.entities.footwear.Shoes;
import org.example.entities.footwear.Sneakers;

public enum ApplianceTypeMapper {
    INSTANCE;

    public Class getApplicationType(String type) {
        switch (type) {
            case "dress" -> {
                return Dress.class;
            }
            case "heels" -> {
                return Heels.class;
            }
            case "pants" -> {
                return Pants.class;
            }
            case "shirt" -> {
                return Shirt.class;
            }
            case "shoes" -> {
                return Shoes.class;
            }
            case "sneakers" -> {
                return Sneakers.class;
            }
            default -> throw new IllegalArgumentException();
        }
    }
}
