package org.example.services;

import org.example.dal.dao.DAOFactory;
import org.example.dal.dao.UsersDAO;
import org.example.dal.search.SearchCriteria;
import org.example.entities.users.User;
import org.example.exceptions.AuthenticationException;

public class AuthenticationServiceImpl<T extends User> extends AuthenticationService<T> {
    private final DAOFactory daoFactoryImpl;
    private T authenticatedUser;
    public AuthenticationServiceImpl(DAOFactory daoFactoryImpl) {
        this.daoFactoryImpl = daoFactoryImpl;
    }

    @Override
    public T authenticateUser(SearchCriteria<T> criteria, String password) throws AuthenticationException {
        UsersDAO<T> dao = daoFactoryImpl.getUserDAO(criteria.getApplianceType());
        if (dao == null) {
            throw new IllegalStateException("No DAO is found for " + criteria.getApplianceType());
        }
        T user = dao.findOne(criteria);
        if (user.getHashedPassword().equals(password)) {
            this.authenticatedUser = user;
            return user;
        }
        throw new AuthenticationException();
    }

    @Override
    public T getAuthenticatedUser() {
        return authenticatedUser;
    }
}
