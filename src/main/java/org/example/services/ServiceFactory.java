package org.example.services;

import org.example.dal.dao.impl.DAOFactoryImpl;
import org.example.entities.users.User;

public enum ServiceFactory {
    INSTANCE;


    public ProductService getProductService() {

        return new ProductServiceImpl(DAOFactoryImpl.INSTANCE);
    }

    public <T extends User> AuthenticationService<T> getAuthenticationService() {

        return new AuthenticationServiceImpl<T>(DAOFactoryImpl.INSTANCE);
    }
}
