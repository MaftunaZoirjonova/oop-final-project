package org.example.services;

import org.example.dal.search.SearchCriteria;
import org.example.entities.Product;

import java.util.Collection;

public abstract class ProductService implements AbstractService {
    public abstract <T extends Product> Collection<T> find(SearchCriteria<T> criteria);
    public abstract <T extends Product> T create(T entity);
    public abstract void delete(int id, String type);
}
