package org.example.services;

import org.example.dal.dao.DAOFactory;
import org.example.dal.dao.ProductDAO;
import org.example.dal.search.SearchCriteria;
import org.example.entities.ApplianceTypeMapper;
import org.example.entities.Product;

import java.util.Collection;

public class ProductServiceImpl extends ProductService {
    private final DAOFactory daoFactoryImpl;
    public ProductServiceImpl(DAOFactory daoFactoryImpl) {
        this.daoFactoryImpl = daoFactoryImpl;
    }

    @Override
    public <T extends Product> Collection<T> find(SearchCriteria<T> criteria) {
        ProductDAO<T> dao = daoFactoryImpl.getProductDAO(criteria.getApplianceType());
        if (dao == null) {
            throw new IllegalStateException("No DAO is found for " + criteria.getApplianceType());
        }
        return dao.find(criteria);
    }

    @Override
    public <T extends Product> T create(T entity) {
        ProductDAO<T> dao = daoFactoryImpl.getProductDAO(entity.getApplianceType());
        return dao.create(entity);
    }

    @Override
    public void delete(int id, String type) {
        Class applianceType = ApplianceTypeMapper.INSTANCE.getApplicationType(type);
        ProductDAO dao = daoFactoryImpl.getProductDAO(applianceType);
        dao.delete(id);
    }
}
