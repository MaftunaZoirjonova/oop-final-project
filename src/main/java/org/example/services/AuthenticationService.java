package org.example.services;

import org.example.dal.search.SearchCriteria;
import org.example.entities.users.User;
import org.example.exceptions.AuthenticationException;

public abstract class AuthenticationService<T> implements AbstractService  {
    public abstract T authenticateUser(SearchCriteria<T> criteria, String password) throws AuthenticationException;
    public abstract T getAuthenticatedUser();
}
