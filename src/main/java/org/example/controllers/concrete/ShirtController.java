package org.example.controllers.concrete;

import org.example.controllers.converters.ParameterConverter;
import org.example.dal.search.SearchCriteria;
import org.example.dal.search.criterias.ShirtSearchCriteria;
import org.example.entities.clothing.Shirt;
import org.example.services.ProductService;

import java.util.Collection;

public class ShirtController extends UserConcreteController<Shirt> {

    public ShirtController(ProductService productService, Collection<ParameterConverter<Shirt>> parameterConverters) {
        super(productService, parameterConverters);
    }

    @Override
    protected SearchCriteria<Shirt> createCriteria() {
        return new ShirtSearchCriteria();
    }
}
