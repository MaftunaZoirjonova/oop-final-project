package org.example.controllers.concrete;

import org.example.controllers.converters.ParameterConverter;
import org.example.dal.search.SearchCriteria;
import org.example.dal.search.criterias.SneakersSearchCriteria;
import org.example.entities.footwear.Sneakers;
import org.example.services.ProductService;

import java.util.Collection;

public class SneakersController extends UserConcreteController<Sneakers> {

    public SneakersController(ProductService productService, Collection<ParameterConverter<Sneakers>> parameterConverters) {
        super(productService, parameterConverters);
    }

    @Override
    protected SearchCriteria<Sneakers> createCriteria() {
        return new SneakersSearchCriteria();
    }
}
