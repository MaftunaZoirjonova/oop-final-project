package org.example.controllers.concrete;

import org.example.controllers.converters.ParameterConversionException;
import org.example.controllers.converters.ParameterConverter;
import org.example.controllers.io.FinishStatus;
import org.example.controllers.io.IOHelper;
import org.example.dal.search.Parameter;
import org.example.dal.search.SearchCriteria;
import org.example.services.AbstractService;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class ConcreteController<A> {
    protected final AbstractService service;

    protected final Map<String, ParameterConverter<A>> parameterConverters;

    public static record Response<A>(FinishStatus status, Collection<A> products) {}

    protected ConcreteController(AbstractService applianceService,
                                 Collection<ParameterConverter<A>> parameterConverters) {
        this.service = Objects.requireNonNull(applianceService);
        this.parameterConverters = Objects.requireNonNull(parameterConverters).stream()
                .collect(Collectors.toMap(
                        ParameterConverter::parameterName,
                        Function.identity()
                ));
    }

    public abstract Response<A> acceptRequest(IOHelper io) throws IOException;

    protected Parameter<A> toParameter(String parameter) throws ParameterConversionException {
        var desc = parameter.split(":");
        var converter = parameterConverters.get(desc[0].strip().toLowerCase());
        if (converter == null) {
            throw new IllegalArgumentException(desc[0].strip());
        }
        return converter.convert(desc[1].strip());
    }

    protected abstract SearchCriteria<A> createCriteria();
}
