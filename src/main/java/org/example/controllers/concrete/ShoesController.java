package org.example.controllers.concrete;

import org.example.controllers.converters.ParameterConverter;
import org.example.dal.search.SearchCriteria;
import org.example.dal.search.criterias.ShoesSearchCriteria;
import org.example.entities.footwear.Shoes;
import org.example.services.ProductService;

import java.util.Collection;

public class ShoesController extends UserConcreteController<Shoes> {

    public ShoesController(ProductService productService, Collection<ParameterConverter<Shoes>> parameterConverters) {
        super(productService, parameterConverters);
    }

    @Override
    protected SearchCriteria<Shoes> createCriteria() {
        return new ShoesSearchCriteria();
    }
}
