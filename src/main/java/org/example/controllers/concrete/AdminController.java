package org.example.controllers.concrete;

import org.example.controllers.converters.ParameterConverter;
import org.example.dal.search.SearchCriteria;
import org.example.dal.search.criterias.UsersSearchCriteria;
import org.example.entities.users.User;
import org.example.services.AuthenticationService;
import org.example.services.ProductService;

import java.util.Collection;

public class AdminController extends AdminConcreteController<User> {

    public AdminController(ProductService applianceService, AuthenticationService<User> authenticationService, Collection<ParameterConverter<User>> parameterConverters) {
        super(applianceService, authenticationService, parameterConverters);
    }

    @Override
    protected SearchCriteria<User> createCriteria() {
        return new UsersSearchCriteria();
    }
}
