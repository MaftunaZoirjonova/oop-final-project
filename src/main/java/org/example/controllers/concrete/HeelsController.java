package org.example.controllers.concrete;

import org.example.controllers.converters.ParameterConverter;
import org.example.dal.search.SearchCriteria;
import org.example.dal.search.criterias.HeelsSearchCriteria;
import org.example.entities.footwear.Heels;
import org.example.services.ProductService;

import java.util.Collection;

public class HeelsController extends UserConcreteController<Heels> {

    public HeelsController(ProductService productService, Collection<ParameterConverter<Heels>> parameterConverters) {
        super(productService, parameterConverters);
    }

    @Override
    protected SearchCriteria<Heels> createCriteria() {
        return new HeelsSearchCriteria();
    }
}
