package org.example.controllers.concrete;

import org.example.controllers.converters.ParameterConverter;
import org.example.dal.search.SearchCriteria;
import org.example.dal.search.criterias.DressSearchCriteria;
import org.example.entities.clothing.Dress;
import org.example.services.ProductService;

import java.util.Collection;

public class DressController extends UserConcreteController<Dress> {

    public DressController(ProductService productService, Collection<ParameterConverter<Dress>> parameterConverters) {
        super(productService, parameterConverters);
    }

    @Override
    protected SearchCriteria<Dress> createCriteria() {
        return new DressSearchCriteria();
    }
}
