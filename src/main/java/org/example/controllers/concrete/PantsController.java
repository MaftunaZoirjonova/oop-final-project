package org.example.controllers.concrete;

import org.example.controllers.converters.ParameterConverter;
import org.example.dal.search.SearchCriteria;
import org.example.dal.search.criterias.PantsSearchCriteria;
import org.example.entities.clothing.Pants;
import org.example.services.ProductService;

import java.util.Collection;

public class PantsController extends UserConcreteController<Pants> {

    public PantsController(ProductService productService, Collection<ParameterConverter<Pants>> parameterConverters) {
        super(productService, parameterConverters);
    }

    @Override
    protected SearchCriteria<Pants> createCriteria() {
        return new PantsSearchCriteria();
    }
}
