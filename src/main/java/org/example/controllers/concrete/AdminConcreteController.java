package org.example.controllers.concrete;

import org.example.controllers.converters.ParameterConversionException;
import org.example.controllers.converters.ParameterConverter;
import org.example.controllers.io.FinishStatus;
import org.example.controllers.io.IOHelper;
import org.example.dal.parser.CsvLineParser;
import org.example.dal.parser.CsvLineParserFactory;
import org.example.dal.search.SearchCriteria;
import org.example.entities.Product;
import org.example.entities.users.AdminRole;
import org.example.entities.users.User;
import org.example.exceptions.AuthenticationException;
import org.example.services.AuthenticationService;
import org.example.services.ProductService;

import java.io.IOException;
import java.util.Collection;

public abstract class AdminConcreteController<A extends User> extends ConcreteController<A> {

    private final AuthenticationService<A> authenticationService;
    protected AdminConcreteController(ProductService applianceService, AuthenticationService<A> authenticationService, Collection<ParameterConverter<A>> parameterConverters) {
        super(applianceService, parameterConverters);
        this.authenticationService = authenticationService;
    }

    @Override
    public Response<A> acceptRequest(IOHelper io) throws IOException {
        if (authenticationService.getAuthenticatedUser() == null) {
            var status = authenticateUser(io);
            if (status != FinishStatus.AUTHENTICATED) {
                return new Response<>(status, null);
            }
        }
        ProductService productService = (ProductService) service;
        User authenticatedUser = authenticationService.getAuthenticatedUser();
        io.println("Hello, " + authenticatedUser.getName());
        io.println("Please type what kind of entity you want to create or delete");
        io.println("Type \"create dress/heels/pants/shirt/shoes/sneakers\" and write params with comma-separate to create a new record");
        io.println("Type \"delete dress/heels/pants/shirt/shoes/sneakers {id}\" to delete record");
        io.println("Type \"cancel\" to leave admin");
        io.println("Templates:");
        io.println("create;dress;id,name,price,category,count,color,size,style");
        io.println("create;heels;id,name,price,category,count,brand,size,heelHeight");
        io.println("create;pants;id,name,price,category,count,color,size,length");
        io.println("create;shirt;id,name,price,category,count,color,size,material");
        io.println("create;shoes;id,name,price,category,count,brand,size,toeBox");
        io.println("create;sneakers;id,name,price,category,count,brand,size,model");
        var status = io.getReader()
                .stopOnStatus(FinishStatus.CANCEL)
                .stopOnStatus(FinishStatus.END_OF_INPUT)
                .doAction(request -> {
                    if (request.startsWith("create")) {
                        String[] splitted = request.split(";");
                        if (splitted.length != 3) {
                            io.error("Invalid command");
                            return FinishStatus.CONTINUE;
                        }
                        String type = splitted[1];
                        String params = splitted[2];
                        CsvLineParser<Product> parser = CsvLineParserFactory.INSTANCE.getProductCsvLineParser(type);
                        Product newProduct = parser.parse(params.split(","));
                        ((ProductService) service).
                                create(newProduct);
                        io.println("Created");
                    } else if (request.startsWith("delete")) {
                        String[] splitted = request.split(" ");
                        if (splitted.length != 3) {
                            io.error("Invalid command");
                            return FinishStatus.CONTINUE;
                        }
                        String type = splitted[1];
                        String id = splitted[2];
                        ((ProductService) service).delete(Integer.parseInt(id), type);
                        io.println("Deleted");
                    } else {
                        io.error("Invalid argument.");
                        io.println("Type \"create dress/heels/pants/shirt/shoes/sneakers\" to create a new record");
                        io.println("Type \"delete dress/heels/pants/shirt/shoes/sneakers {id}\" to delete record");
                    }
                    return FinishStatus.CONTINUE;
                }).read();
        return new Response<>(status, null);
    }

    private FinishStatus authenticateUser(IOHelper io) throws IOException {
        SearchCriteria<A> criteria = createCriteria();
        io.print("Type username and password with space: ");
        return io.getReader()
                .stopOnStatus(FinishStatus.AUTHENTICATED)
                .stopOnStatus(FinishStatus.CANCEL)
                .stopOnStatus(FinishStatus.END_OF_INPUT)
                .doAction(request -> {
                    String[] splitedRequest = request.split(" ");
                    if (splitedRequest.length != 2) {
                        io.error("Invalid parameter. Type username and password with space.");
                    }
                    try {
                        criteria.add(toParameter("username:" + splitedRequest[0]));
                    } catch (ParameterConversionException e) {
                        io.error(e.getMessage());
                    }
                    try {
                        User user = authenticationService.authenticateUser(criteria, splitedRequest[1]);
                        if (user.hasRole(new AdminRole())) {
                            return FinishStatus.AUTHENTICATED;
                        }
                        io.error("Sorry, this section only for admins :(");
                    } catch (AuthenticationException e) {
                        io.error("User not found or password is incorrect");
                    }
                    return FinishStatus.CONTINUE;
                }).read();
    }
}
