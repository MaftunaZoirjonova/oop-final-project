package org.example.controllers.concrete;

import org.example.controllers.converters.ParameterConversionException;
import org.example.controllers.converters.ParameterConverter;
import org.example.controllers.io.FinishStatus;
import org.example.controllers.io.IOHelper;
import org.example.dal.search.Parameter;
import org.example.dal.search.SearchCriteria;
import org.example.entities.Product;
import org.example.services.ProductService;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

public abstract class UserConcreteController<A extends Product> extends ConcreteController<A> {
    protected UserConcreteController(ProductService applianceService, Collection<ParameterConverter<A>> collection) {
        super(applianceService, collection);
    }

    @Override
    public Response<A> acceptRequest(IOHelper io) throws IOException {
        SearchCriteria<A> criteria = createCriteria();
        io.print("Type search criteria in format property:criteria (for example, price:10-50) or \"all\" to list all products: ");
        var status = io.getReader()
                .stopOnStatus(FinishStatus.END_OF_INPUT)
                .stopOnStatus(FinishStatus.SEARCH)
                .stopOnStatus(FinishStatus.CANCEL)
                .stopOnStatus(FinishStatus.COUNT)
                .doAction(request -> {
                    if (request.equals("all")) {
                        criteria.add(Parameter.any());
                        return FinishStatus.SEARCH;
                    } else {
                        try {
                            criteria.add(toParameter(request));
                        } catch (IllegalArgumentException e) {
                            io.error("Invalid parameter: " + e.getMessage());
                        } catch (ParameterConversionException e) {
                            io.error(e.getMessage());
                        }
                    }
                    if (criteria.getParameters().size() == 1) {
                        io.println("Continue typing search criteria or commands \"search\" to print search results");
                        io.println("and \"count\" to print count of found products");
                        io.println("Type \"cancel\" command to cancel search and back to main menu");
                    }
                    io.print("Waiting for input: ");
                    return FinishStatus.CONTINUE;
                })
                .read();
        return new Response<>(status, status.isSuccess() ? ((ProductService)service).find(criteria) : List.of());
    }
}
