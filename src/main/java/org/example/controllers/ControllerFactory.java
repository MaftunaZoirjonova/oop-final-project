package org.example.controllers;

import org.example.config.RawConverters;
import org.example.controllers.concrete.*;
import org.example.controllers.io.IOHelper;
import org.example.entities.users.User;
import org.example.services.AuthenticationService;
import org.example.services.ProductService;

import java.util.List;
import java.util.Map;

public enum ControllerFactory {
    INSTANCE;

    public DispatcherController dispatcherController(ProductService productService, AuthenticationService<User> authenticationService) {
        return new DispatcherController(
                IOHelper.system(),
                Map.of(
                        "shirts", new ShirtController(productService, List.of(
                                RawConverters.NAME.generic(),
                                RawConverters.PRICE.generic(),
                                RawConverters.CLOTHING_SIZE.generic(),
                                RawConverters.CATEGORY.generic(),
                                RawConverters.MATERIAL.generic(),
                                RawConverters.COUNT.generic(),
                                RawConverters.COLOR.generic()
                        )),
                        "dress", new DressController(productService, List.of(
                                RawConverters.NAME.generic(),
                                RawConverters.PRICE.generic(),
                                RawConverters.CLOTHING_SIZE.generic(),
                                RawConverters.CATEGORY.generic(),
                                RawConverters.STYLE.generic(),
                                RawConverters.COUNT.generic(),
                                RawConverters.COLOR.generic()
                        )),
                        "pants", new PantsController(productService, List.of(
                                RawConverters.NAME.generic(),
                                RawConverters.PRICE.generic(),
                                RawConverters.CLOTHING_SIZE.generic(),
                                RawConverters.CATEGORY.generic(),
                                RawConverters.PANTS_LENGTHS.generic(),
                                RawConverters.COUNT.generic(),
                                RawConverters.COLOR.generic()
                        )),
                        "sneakers", new SneakersController(productService, List.of(
                                RawConverters.NAME.generic(),
                                RawConverters.PRICE.generic(),
                                RawConverters.CATEGORY.generic(),
                                RawConverters.COUNT.generic(),
                                RawConverters.BRAND.generic(),
                                RawConverters.FOOT_SIZE.generic()
                        )),
                        "heels", new HeelsController(productService, List.of(
                                RawConverters.NAME.generic(),
                                RawConverters.PRICE.generic(),
                                RawConverters.CATEGORY.generic(),
                                RawConverters.COUNT.generic(),
                                RawConverters.BRAND.generic(),
                                RawConverters.FOOT_SIZE.generic()
                        )),
                        "shoes", new ShoesController(productService, List.of(
                                RawConverters.NAME.generic(),
                                RawConverters.PRICE.generic(),
                                RawConverters.CATEGORY.generic(),
                                RawConverters.COUNT.generic(),
                                RawConverters.BRAND.generic(),
                                RawConverters.FOOT_SIZE.generic()
                        )),
                        "admin", new AdminController(productService, authenticationService, List.of(
                                RawConverters.USERNAME.generic(),
                                RawConverters.ID.generic()
                        ))
                )
        );
    }
}
