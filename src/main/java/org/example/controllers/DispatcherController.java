package org.example.controllers;

import org.example.controllers.concrete.ConcreteController;
import org.example.controllers.io.FinishStatus;
import org.example.controllers.io.IOHelper;
import org.example.entities.Product;

import java.io.Closeable;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;

public class DispatcherController implements Closeable {
    private final IOHelper io;
    private final Map<String, ConcreteController<?>> controllers;

    public DispatcherController(IOHelper io, Map<String, ConcreteController<?>> controllers) {
        this.io = Objects.requireNonNull(io);
        this.controllers = Objects.requireNonNull(controllers);
    }

    public void listen() throws IOException {
        io.println("Clothes and Footwear Warehouse Search System");
        io.println("Developer:");
        io.println("\tName: Zoirjonova Maftuna");
        io.println("\tEmail: maftuna_zoirjonova@student.itpu.uz");
        io.println("\n");
        io.println("Type command \"quit\" at any place of program to quit");
        io.print("Please, type what kind of product do you want to find (\"shirts\", \"dress\", \"pants\", \"sneakers\", \"heels\", \"shoes\") or \"admin\" to login in Admin Dashboard: ");
        io.getReader()
                .stopOnStatus(FinishStatus.END_OF_INPUT)
                .doAction(request -> {
                    ConcreteController<?> controller = controllers.get(request.toLowerCase());
                    if (controller == null) {
                        io.error("Unsupported product type: " + request);
                        return FinishStatus.CONTINUE;
                    }
                    ConcreteController.Response<?> response = controller.acceptRequest(io);
                    switch (response.status()) {
                        case END_OF_INPUT -> {
                            io.println("Bye!");
                            return FinishStatus.END_OF_INPUT;
                        }
                        case SEARCH -> {
                            if (response.products().isEmpty()) {
                                io.println("Products with such criteria not found");
                            } else {
                                response.products().forEach(io::println);
                            }
                        }
                        case COUNT ->
                                io.println("Count of found products: " + response.products().stream().mapToInt((p) -> ((Product) p).getCount()).sum());
                    }
                    io.print("Please, type what kind of product do you want to find (shirts, dress, pants, sneakers, heels, shoes): ");
                    return FinishStatus.CONTINUE;
                }).read();
    }

    @Override
    public void close() throws IOException {
        io.close();
    }
}
