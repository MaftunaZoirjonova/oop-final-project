package org.example.controllers.converters;

public class ParameterConversionException extends Exception {
    public ParameterConversionException(String message) {
        super(message);
    }
}
