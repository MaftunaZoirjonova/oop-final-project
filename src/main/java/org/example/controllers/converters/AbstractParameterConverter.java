package org.example.controllers.converters;

import org.example.dal.search.Parameter;
import org.example.dal.search.parametrs.InvalidParameterArguments;
import org.example.entities.Product;

public abstract class AbstractParameterConverter<A> implements ParameterConverter<A> {
    private final String parameterName;

    protected AbstractParameterConverter(String parameterName) {
        this.parameterName = parameterName;
    }

    @Override
    public Parameter<A> convert(String request) throws ParameterConversionException {
        try {
            return internalConvert(request);
        } catch (NumberFormatException e) {
            throw new ParameterConversionException("The passed value is not a number: " + request);
        }
        catch (InvalidParameterArguments e) {
            throw new ParameterConversionException("Request '" + request + "' is invalid because " + e.getMessage());
        }
    }

    @Override
    public String parameterName() {
        return parameterName;
    }

    protected abstract Parameter<A> internalConvert(String request) throws ParameterConversionException;
}
