package org.example.controllers.converters.impl;

import org.example.controllers.converters.AbstractParameterConverter;
import org.example.controllers.converters.ParameterConversionException;
import org.example.dal.search.Parameter;
import org.example.dal.search.parametrs.CountParameter;
import org.example.entities.Product;

public class CountParameterConverter extends AbstractParameterConverter<Product> {

    public CountParameterConverter() {
        super("count");
    }

    @Override
    protected Parameter<Product> internalConvert(String request) throws ParameterConversionException {
        int i = 0;
        while (i < request.length() && Character.isWhitespace(request.charAt(i))) {
            i++;
        }
        request = request.substring(i);
        String[] splitRequest = request.split(" ");
        if (splitRequest.length < 2) {
            throw new ParameterConversionException("Wrong value of count parameter: " + request);
        }
        return new CountParameter(splitRequest[0], Integer.parseInt(splitRequest[1]));
    }
}
