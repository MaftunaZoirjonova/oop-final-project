package org.example.controllers.converters.impl;

import org.example.controllers.converters.AbstractParameterConverter;
import org.example.controllers.converters.ParameterConversionException;
import org.example.dal.search.Parameter;
import org.example.dal.search.parametrs.SneakersModelParameter;
import org.example.entities.footwear.Sneakers;

public class SneakersModelParameterConverter extends AbstractParameterConverter<Sneakers> {

    public SneakersModelParameterConverter() {
        super("material");
    }

    @Override
    protected Parameter<Sneakers> internalConvert(String request) throws ParameterConversionException {
        return new SneakersModelParameter(request);
    }
}
