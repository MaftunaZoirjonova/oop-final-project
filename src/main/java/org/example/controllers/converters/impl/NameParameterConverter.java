package org.example.controllers.converters.impl;

import org.example.controllers.converters.AbstractParameterConverter;
import org.example.controllers.converters.ParameterConversionException;
import org.example.dal.search.Parameter;
import org.example.dal.search.parametrs.NameParameter;
import org.example.entities.Product;

public class NameParameterConverter extends AbstractParameterConverter<Product> {

    public NameParameterConverter() {
        super("name");
    }

    @Override
    protected Parameter<Product> internalConvert(String request) throws ParameterConversionException {
        return new NameParameter(request);
    }
}
