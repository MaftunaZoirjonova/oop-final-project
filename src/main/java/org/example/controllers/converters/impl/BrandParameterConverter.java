package org.example.controllers.converters.impl;

import org.example.controllers.converters.AbstractParameterConverter;
import org.example.controllers.converters.ParameterConversionException;
import org.example.dal.search.Parameter;
import org.example.dal.search.parametrs.BrandParameter;
import org.example.entities.footwear.FootWear;

public class BrandParameterConverter extends AbstractParameterConverter<FootWear> {

    public BrandParameterConverter() {
        super("brand");
    }

    @Override
    protected Parameter<FootWear> internalConvert(String request) throws ParameterConversionException {
        return new BrandParameter(request);
    }
}
