package org.example.controllers.converters.impl;

import org.example.controllers.converters.AbstractParameterConverter;
import org.example.controllers.converters.ParameterConversionException;
import org.example.dal.search.Parameter;
import org.example.dal.search.parametrs.DressStyleParameter;
import org.example.entities.clothing.Dress;

public class DressStyleParameterConverter extends AbstractParameterConverter<Dress> {

    public DressStyleParameterConverter() {
        super("style");
    }

    @Override
    protected Parameter<Dress> internalConvert(String request) throws ParameterConversionException {
        return new DressStyleParameter(request);
    }
}
