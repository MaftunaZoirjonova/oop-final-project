package org.example.controllers.converters.impl;

import org.example.controllers.converters.AbstractParameterConverter;
import org.example.controllers.converters.ParameterConversionException;
import org.example.dal.search.Parameter;
import org.example.dal.search.parametrs.ShirtMaterialParameter;
import org.example.entities.clothing.Shirt;

public class ShirtMaterialParameterConverter extends AbstractParameterConverter<Shirt> {

    public ShirtMaterialParameterConverter() {
        super("material");
    }

    @Override
    protected Parameter<Shirt> internalConvert(String request) throws ParameterConversionException {
        return new ShirtMaterialParameter(request);
    }
}
