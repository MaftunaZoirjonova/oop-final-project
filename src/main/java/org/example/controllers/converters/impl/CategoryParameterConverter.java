package org.example.controllers.converters.impl;

import org.example.controllers.converters.AbstractParameterConverter;
import org.example.controllers.converters.ParameterConversionException;
import org.example.dal.search.Parameter;
import org.example.dal.search.parametrs.CategoryParameter;
import org.example.entities.Product;

public class CategoryParameterConverter extends AbstractParameterConverter<Product> {

    public CategoryParameterConverter() {
        super("category");
    }

    @Override
    protected Parameter<Product> internalConvert(String request) throws ParameterConversionException {
        return new CategoryParameter(request);
    }
}
