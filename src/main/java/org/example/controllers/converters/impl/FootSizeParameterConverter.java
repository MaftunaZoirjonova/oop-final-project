package org.example.controllers.converters.impl;

import org.example.controllers.converters.AbstractParameterConverter;
import org.example.controllers.converters.ParameterConversionException;
import org.example.dal.search.Parameter;
import org.example.dal.search.parametrs.FootSizeParameter;
import org.example.entities.footwear.FootWear;

public class FootSizeParameterConverter extends AbstractParameterConverter<FootWear> {

    public FootSizeParameterConverter() {
        super("size");
    }

    @Override
    protected Parameter<FootWear> internalConvert(String request) throws ParameterConversionException {
        return new FootSizeParameter(Double.parseDouble(request));
    }
}
