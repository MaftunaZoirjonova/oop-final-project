package org.example.controllers.converters.impl;

import org.example.controllers.converters.AbstractParameterConverter;
import org.example.dal.search.Parameter;
import org.example.dal.search.parametrs.PriceParameter;
import org.example.dal.search.parametrs.Range;
import org.example.entities.Product;

public class PriceParameterConverter extends AbstractParameterConverter<Product> {

    public PriceParameterConverter() {
        super("price");
    }

    @Override
    protected Parameter<Product> internalConvert(String request) {
        var value = request.split("-", 2);
        Range<Long> range = value.length == 1
                ? new Range<>(Long.parseLong(value[0]), Long.parseLong(value[0]))
                : new Range<>(Long.parseLong(value[0]), Long.parseLong(value[1]));
        return new PriceParameter(range);
    }
}
