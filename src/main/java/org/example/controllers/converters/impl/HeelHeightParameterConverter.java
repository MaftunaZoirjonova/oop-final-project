package org.example.controllers.converters.impl;

import org.example.controllers.converters.AbstractParameterConverter;
import org.example.controllers.converters.ParameterConversionException;
import org.example.dal.search.Parameter;
import org.example.dal.search.parametrs.HeelHeightParameter;
import org.example.entities.footwear.Heels;

public class HeelHeightParameterConverter extends AbstractParameterConverter<Heels> {

    public HeelHeightParameterConverter() {
        super("heel_height");
    }

    @Override
    protected Parameter<Heels> internalConvert(String request) throws ParameterConversionException {
        return new HeelHeightParameter(Integer.parseInt(request));
    }
}
