package org.example.controllers.converters.impl;

import org.example.controllers.converters.AbstractParameterConverter;
import org.example.controllers.converters.ParameterConversionException;
import org.example.dal.search.Parameter;
import org.example.dal.search.parametrs.IdParameter;
import org.example.entities.Product;

public class IdParameterConverter extends AbstractParameterConverter<Product> {

    public IdParameterConverter() {
        super("id");
    }

    @Override
    protected Parameter<Product> internalConvert(String request) throws ParameterConversionException {
        return new IdParameter(Integer.parseInt(request));
    }
}
