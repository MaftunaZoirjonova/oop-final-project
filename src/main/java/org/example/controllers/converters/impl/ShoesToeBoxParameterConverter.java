package org.example.controllers.converters.impl;

import org.example.controllers.converters.AbstractParameterConverter;
import org.example.controllers.converters.ParameterConversionException;
import org.example.dal.search.Parameter;
import org.example.dal.search.parametrs.ShoesToeBoxParameter;
import org.example.entities.footwear.Shoes;

public class ShoesToeBoxParameterConverter extends AbstractParameterConverter<Shoes> {

    public ShoesToeBoxParameterConverter() {
        super("size");
    }

    @Override
    protected Parameter<Shoes> internalConvert(String request) throws ParameterConversionException {
        return new ShoesToeBoxParameter(Shoes.ToeBox.valueOf(request));
    }
}
