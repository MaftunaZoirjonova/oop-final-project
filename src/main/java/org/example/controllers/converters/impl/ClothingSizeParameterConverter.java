package org.example.controllers.converters.impl;

import org.example.controllers.converters.AbstractParameterConverter;
import org.example.controllers.converters.ParameterConversionException;
import org.example.dal.search.Parameter;
import org.example.dal.search.parametrs.ClothingSizeParameter;
import org.example.entities.clothing.Clothing;

public class ClothingSizeParameterConverter extends AbstractParameterConverter<Clothing> {

    public ClothingSizeParameterConverter() {
        super("size");
    }

    @Override
    protected Parameter<Clothing> internalConvert(String request) throws ParameterConversionException {
        return new ClothingSizeParameter(Clothing.Size.valueOf(request));
    }
}
