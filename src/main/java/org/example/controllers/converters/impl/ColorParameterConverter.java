package org.example.controllers.converters.impl;

import org.example.controllers.converters.AbstractParameterConverter;
import org.example.controllers.converters.ParameterConversionException;
import org.example.dal.search.Parameter;
import org.example.dal.search.parametrs.ColorParameter;
import org.example.entities.clothing.Clothing;

public class ColorParameterConverter extends AbstractParameterConverter<Clothing> {

    public ColorParameterConverter() {
        super("color");
    }

    @Override
    protected Parameter<Clothing> internalConvert(String request) throws ParameterConversionException {
        return new ColorParameter(request);
    }
}
