package org.example.controllers.converters.impl;

import org.example.controllers.converters.AbstractParameterConverter;
import org.example.controllers.converters.ParameterConversionException;
import org.example.dal.search.Parameter;
import org.example.dal.search.parametrs.PantsLengthParameter;
import org.example.entities.clothing.Pants;

public class PantsLengthParameterConverter extends AbstractParameterConverter<Pants> {
    public PantsLengthParameterConverter() {
        super("length");
    }
    @Override
    protected Parameter<Pants> internalConvert(String request) throws ParameterConversionException {
        return new PantsLengthParameter(Integer.parseInt(request));
    }
}
