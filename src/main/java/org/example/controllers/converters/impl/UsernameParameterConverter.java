package org.example.controllers.converters.impl;

import org.example.controllers.converters.AbstractParameterConverter;
import org.example.controllers.converters.ParameterConversionException;
import org.example.dal.search.Parameter;
import org.example.dal.search.parametrs.UsernameParameter;
import org.example.entities.users.User;

public class UsernameParameterConverter extends AbstractParameterConverter<User> {
    public UsernameParameterConverter() {
        super("username");
    }

    @Override
    protected Parameter<User> internalConvert(String request) throws ParameterConversionException {
        return new UsernameParameter(request);
    }
}
