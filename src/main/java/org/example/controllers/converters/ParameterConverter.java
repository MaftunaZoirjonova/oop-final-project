package org.example.controllers.converters;

import org.example.dal.search.Parameter;
import org.example.entities.Product;

public interface ParameterConverter<A> {
    Parameter<A> convert(String request) throws ParameterConversionException;
    String parameterName();
}
