package org.example.controllers.io;

import java.io.*;
import java.util.Objects;

public class IOHelper implements Closeable {
    private final InputStream in;
    private final PrintStream out;
    private final PrintStream err;
    private BufferedReader openedIn;

    public IOHelper(InputStream in, OutputStream out, OutputStream err) {
        this.in = Objects.requireNonNull(in);
        this.out = new PrintStream(Objects.requireNonNull(out), true);
        this.err = new PrintStream(Objects.requireNonNull(err), true);
    }

    public static IOHelper system() {
        return new IOHelper(System.in, System.out, System.err);
    }

    void setOpenedIn(BufferedReader reader) {
        this.openedIn = reader;
    }

    public InputReader.Builder getReader() {
        if (openedIn != null) {
            return InputReader.from(this, openedIn);
        }
        else {
            return InputReader.from(this, in);
        }
    }

    public void println(Object msg) {
        out.println(msg);
    }

    public void print(Object msg) {
        out.print(msg);
    }

    public void error(Object msg) {
        err.println(msg);
    }

    @Override
    public void close() throws IOException {
        in.close();
        out.close();
        err.close();
    }
}
